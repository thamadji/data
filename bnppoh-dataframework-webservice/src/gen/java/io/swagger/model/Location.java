package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Location
 **/

@ApiModel(description = "Location")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public class Location {

	private String id = null;
	private String name = null;
	private String address = null;
	private Double latitude = null;
	private Double longitude = null;
	private String zipCode = null;
	private String website = null;
	private String phoneNumber = null;
	private String openingSchedule = null;

	/**
	 * Unique ID
	 **/

	@ApiModelProperty(value = "Unique ID")
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Name
	 **/

	@ApiModelProperty(value = "Name")
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Address
	 **/

	@ApiModelProperty(value = "Address")
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * Latitude
	 **/

	@ApiModelProperty(value = "Latitude")
	@JsonProperty("latitude")
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(final Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Longitude
	 **/

	@ApiModelProperty(value = "Longitude")
	@JsonProperty("longitude")
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Zip code
	 **/

	@ApiModelProperty(value = "Zip code")
	@JsonProperty("zipCode")
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(final String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Website
	 **/

	@ApiModelProperty(value = "Website")
	@JsonProperty("website")
	public String getWebsite() {
		return website;
	}

	public void setWebsite(final String website) {
		this.website = website;
	}

	/**
	 * Phone number
	 **/

	@ApiModelProperty(value = "Phone number")
	@JsonProperty("phoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Openning schedule
	 **/

	@ApiModelProperty(value = "Openning schedule")
	@JsonProperty("openingSchedule")
	public String getOpeningSchedule() {
		return openingSchedule;
	}

	public void setOpeningSchedule(final String openingSchedule) {
		this.openingSchedule = openingSchedule;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Location location = (Location) o;
		return Objects.equals(id, location.id) && Objects.equals(name, location.name)
				&& Objects.equals(address, location.address) && Objects.equals(latitude, location.latitude)
				&& Objects.equals(longitude, location.longitude) && Objects.equals(zipCode, location.zipCode)
				&& Objects.equals(website, location.website) && Objects.equals(phoneNumber, location.phoneNumber)
				&& Objects.equals(openingSchedule, location.openingSchedule);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, address, latitude, longitude, zipCode, website, phoneNumber, openingSchedule);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("class Location {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    address: ").append(toIndentedString(address)).append("\n");
		sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
		sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
		sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
		sb.append("    website: ").append(toIndentedString(website)).append("\n");
		sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
		sb.append("    openingSchedule: ").append(toIndentedString(openingSchedule)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
