package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-10T17:07:19.144Z")
public class UserCoupon   {
  
  private String couponId = null;
  private String individualContactId = null;
  private String status = null;

  
  /**
   **/
  
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("couponId")
  public String getCouponId() {
    return couponId;
  }
  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  
  /**
   **/
  
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("individualContactId")
  public String getIndividualContactId() {
    return individualContactId;
  }
  public void setIndividualContactId(String individualContactId) {
    this.individualContactId = individualContactId;
  }

  
  /**
   **/
  
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserCoupon userCoupon = (UserCoupon) o;
    return Objects.equals(couponId, userCoupon.couponId) &&
        Objects.equals(individualContactId, userCoupon.individualContactId) &&
        Objects.equals(status, userCoupon.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(couponId, individualContactId, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserCoupon {\n");
    
    sb.append("    couponId: ").append(toIndentedString(couponId)).append("\n");
    sb.append("    individualContactId: ").append(toIndentedString(individualContactId)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

