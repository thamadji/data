package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-02-03T11:32:01.484Z")
public class Device {

	private String deviceId = null;
	private Localization localization = null;

	/**
	 **/

	@ApiModelProperty(value = "")
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}

	public void setId(final String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 **/

	@ApiModelProperty(value = "")
	@JsonProperty("localization")
	public Localization getLocalization() {
		return localization;
	}

	public void setLocalization(final Localization localization) {
		this.localization = localization;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Device device = (Device) o;
		return Objects.equals(deviceId, device.deviceId) && Objects.equals(localization, device.localization);
	}

	@Override
	public int hashCode() {
		return Objects.hash(deviceId, localization);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("class Device {\n");

		sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
		sb.append("    localization: ").append(toIndentedString(localization)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
