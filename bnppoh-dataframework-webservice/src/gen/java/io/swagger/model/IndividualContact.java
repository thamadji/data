package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-28T14:54:16.509Z")
public class IndividualContact {

	private String id = null;
	private String firstName = null;
	private String lastName = null;
	private String email = null;
	private String mobilePhoneNumber = null;
	private String birthDate = null;

	// private String deviceId = null;
	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	@ApiModelProperty(required = false, value = "")
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@ApiModelProperty(required = false, value = "")
	@JsonProperty("mobilePhoneNumber")
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(final String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	@ApiModelProperty(required = false, value = "")
	@JsonProperty("birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(final String birthDate) {
		this.birthDate = birthDate;
	}

	// @ApiModelProperty(required = false, value = "")
	// @JsonProperty("deviceId")
	// public String getDeviceId() {
	// return deviceId;
	// }
	//
	// public void setDeviceId(String deviceId) {
	// this.deviceId = deviceId;
	// }

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final IndividualContact individualContact = (IndividualContact) o;
		return Objects.equals(id, individualContact.id) && Objects.equals(firstName, individualContact.firstName)
				&& Objects.equals(lastName, individualContact.lastName)
				&& Objects.equals(email, individualContact.email)
				&& Objects.equals(mobilePhoneNumber, individualContact.mobilePhoneNumber)
				&& Objects.equals(birthDate, individualContact.birthDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, email, mobilePhoneNumber, birthDate);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("class IndividualContact {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    mobilePhoneNumber: ").append(toIndentedString(mobilePhoneNumber)).append("\n");
		sb.append("    age: ").append(toIndentedString(birthDate)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
