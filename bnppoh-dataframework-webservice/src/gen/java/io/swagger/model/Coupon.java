package io.swagger.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Coupon
 **/

@ApiModel(description = "Coupon")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public class Coupon {

	private String id = null;
	private String description = null;
	private String title = null;
	private String price = null;
	private String discountedPrice = null;
	private String discountPercentage = null;
	private String discountConditions = null;
	private String expiryDate = null;
	private List<String> imageURIs = new ArrayList<String>();
	private Location location = null;

	/**
	 * Unique ID
	 **/

	@ApiModelProperty(value = "Unique ID")
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Description
	 **/

	@ApiModelProperty(value = "Description")
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Title
	 **/

	@ApiModelProperty(value = "Title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Price
	 **/

	@ApiModelProperty(value = "Price")
	@JsonProperty("price")
	public String getPrice() {
		return price;
	}

	public void setPrice(final String price) {
		this.price = price;
	}

	/**
	 * Discounted price
	 **/

	@ApiModelProperty(value = "Discounted price")
	@JsonProperty("discountedPrice")
	public String getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(final String discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	/**
	 * Discount percentage
	 **/

	@ApiModelProperty(value = "Discount percentage")
	@JsonProperty("discountPercentage")
	public String getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(final String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	/**
	 * Discount conditions
	 **/

	@ApiModelProperty(value = "Discount conditions")
	@JsonProperty("discountConditions")
	public String getDiscountConditions() {
		return discountConditions;
	}

	public void setDiscountConditions(final String discountConditions) {
		this.discountConditions = discountConditions;
	}

	/**
	 * Expiry date
	 **/

	@ApiModelProperty(value = "Expiry date")
	@JsonProperty("expiryDate")
	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(final String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 **/

	@ApiModelProperty(value = "")
	@JsonProperty("imageURIs")
	public List<String> getImageURIs() {
		return imageURIs;
	}

	public void setImageURIs(final List<String> imageURIs) {
		this.imageURIs = imageURIs;
	}

	/**
	 **/

	@ApiModelProperty(value = "")
	@JsonProperty("location")
	public Location getLocation() {
		return location;
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Coupon coupon = (Coupon) o;
		return Objects.equals(id, coupon.id) && Objects.equals(description, coupon.description)
				&& Objects.equals(title, coupon.title) && Objects.equals(price, coupon.price)
				&& Objects.equals(discountedPrice, coupon.discountedPrice)
				&& Objects.equals(discountPercentage, coupon.discountPercentage)
				&& Objects.equals(discountConditions, coupon.discountConditions)
				&& Objects.equals(expiryDate, coupon.expiryDate) && Objects.equals(imageURIs, coupon.imageURIs)
				&& Objects.equals(location, coupon.location);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, description, title, price, discountedPrice, discountPercentage, discountConditions,
				expiryDate, imageURIs, location);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("class Coupon {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    title: ").append(toIndentedString(title)).append("\n");
		sb.append("    price: ").append(toIndentedString(price)).append("\n");
		sb.append("    discountedPrice: ").append(toIndentedString(discountedPrice)).append("\n");
		sb.append("    discountPercentage: ").append(toIndentedString(discountPercentage)).append("\n");
		sb.append("    discountConditions: ").append(toIndentedString(discountConditions)).append("\n");
		sb.append("    expiryDate: ").append(toIndentedString(expiryDate)).append("\n");
		sb.append("    imageURIs: ").append(toIndentedString(imageURIs)).append("\n");
		sb.append("    location: ").append(toIndentedString(location)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
