package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public class AdressContact {

	private String streetName = null;
	private String cityName = null;
	private String cityZipcode = null;

	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("streetName")
	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(final String streetName) {
		this.streetName = streetName;
	}

	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("cityName")
	public String getCityName() {
		return cityName;
	}

	public void setCityName(final String cityName) {
		this.cityName = cityName;
	}

	/**
	 **/

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("cityZipcode")
	public String getCityZipcode() {
		return cityZipcode;
	}

	public void setCityZipcode(final String cityZipcode) {
		this.cityZipcode = cityZipcode;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final AdressContact adressContact = (AdressContact) o;
		return Objects.equals(streetName, adressContact.streetName) && Objects.equals(cityName, adressContact.cityName)
				&& Objects.equals(cityZipcode, adressContact.cityZipcode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(streetName, cityName, cityZipcode);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("class AdressContact {\n");

		sb.append("    streetName: ").append(toIndentedString(streetName)).append("\n");
		sb.append("    cityName: ").append(toIndentedString(cityName)).append("\n");
		sb.append("    cityZipcode: ").append(toIndentedString(cityZipcode)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
