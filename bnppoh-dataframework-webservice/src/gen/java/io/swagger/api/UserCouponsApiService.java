package io.swagger.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.model.UserCoupon;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public abstract class UserCouponsApiService {

	public abstract Response userCouponsPost(UserCoupon body, String dateSourceAppel, String deviceId, String ipSource,
			SecurityContext securityContext) throws NotFoundException;

	public abstract Response userCouponsArchivedUserCouponIdPut(String userCouponId, String sid, String dateSourceAppel,
			String ipSource, String idDevice, String latitudeDevice, String longitudeDevice,
			String typeProjectionLocalisation, String identifiantApplicationSource, String nomApplicationSource,
			String versionApplicationSource, String identifiantUtilisateurFinal, String ressourcesParametreN,
			String dateRessourceAncienneVersion, SecurityContext securityContext) throws NotFoundException;

	public abstract Response userCouponsClippedUserCouponIdPut(String userCouponId, String sid, String dateSourceAppel,
			String ipSource, String idDevice, String latitudeDevice, String longitudeDevice,
			String typeProjectionLocalisation, String identifiantApplicationSource, String nomApplicationSource,
			String versionApplicationSource, String identifiantUtilisateurFinal, String ressourcesParametreN,
			String dateRessourceAncienneVersion, SecurityContext securityContext) throws NotFoundException;

	public abstract Response userCouponsUserCouponIdGet(String userCouponId, String sid, String dateSourceAppel,
			String ipSource, String idDevice, String latitudeDevice, String longitudeDevice,
			String typeProjectionLocalisation, String identifiantApplicationSource, String nomApplicationSource,
			String versionApplicationSource, String identifiantUtilisateurFinal, String ressourcesParametreN,
			String dateRessourceAncienneVersion, SecurityContext securityContext) throws NotFoundException;

}
