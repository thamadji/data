package io.swagger.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public abstract class CouponsApiService {

	public abstract Response couponsCouponIdGet(String couponId, String sid, String dateSourceAppel, String ipSource,
			String idDevice, String latitudeDevice, String longitudeDevice, String typeProjectionLocalisation,
			String identifiantApplicationSource, String nomApplicationSource, String versionApplicationSource,
			String identifiantUtilisateurFinal, String ressourcesParametreN, String dateRessourceAncienneVersion,
			SecurityContext securityContext) throws NotFoundException;

}
