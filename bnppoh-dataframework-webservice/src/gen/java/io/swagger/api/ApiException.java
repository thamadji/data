package io.swagger.api;

@SuppressWarnings("serial")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public class ApiException extends Exception {
	private final int code;

	public ApiException(final int _code, final String msg) {
		super(msg);
		this.code = _code;
	}
}
