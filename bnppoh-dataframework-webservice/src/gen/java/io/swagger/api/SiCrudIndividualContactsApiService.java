package io.swagger.api;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.model.IndividualContact;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public abstract class SiCrudIndividualContactsApiService {

	public abstract Response siCrudIndividualContactsGet(List<String> timestamp, String dateSourceAppel,
			String ipSource, List<String> firstName, List<String> lastName, SecurityContext securityContext)
					throws NotFoundException;

	public abstract Response siCrudIndividualContactsPost(IndividualContact body, String deviceId,
			String dateSourceAppel, String ipSource, SecurityContext securityContext) throws NotFoundException;

	public abstract Response siCrudIndividualContactsIdGet(String id, String dateSourceAppel, String ipSource,
			SecurityContext securityContext) throws NotFoundException;

	public abstract Response siCrudIndividualContactsIdPut(String id, String dateSourceAppel, String ipSource,
			IndividualContact body, SecurityContext securityContext) throws NotFoundException;

	public abstract Response siCrudIndividualContactsIdDelete(String id, String dateSourceAppel, String ipSource,
			SecurityContext securityContext) throws NotFoundException;

	public abstract Response siCrudIndividualContactsIdPatch(String id, String deviceId, String dateSourceAppel,
			String ipSource, IndividualContact body, SecurityContext securityContext) throws NotFoundException;

}
