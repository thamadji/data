package io.swagger.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.annotations.ApiParam;
import io.swagger.api.factories.UserCouponsApiServiceFactory;
import io.swagger.model.UserCoupon;

@Path("/user_coupons")
@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
@io.swagger.annotations.Api(description = "the user_coupons API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public class UserCouponsApi {
	private final UserCouponsApiService delegate = UserCouponsApiServiceFactory.getUserCouponsApi();

	@POST

	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "Creates a new user coupon in the system", response = void.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 201, message = "Individual Contact response", response = void.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = void.class) })

	public Response userCouponsPost(
			@ApiParam(value = "The object to be saved in the database", required = true) final UserCoupon body,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'identifiant du device à la creation du user coupon") @HeaderParam("deviceId") final String deviceId,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.userCouponsPost(body, dateSourceAppel, deviceId, ipSource, securityContext);
	}

	@PUT
	@Path("/archived/{userCouponId}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "Move user coupon to archived list.", response = void.class, tags = {
			"user coupon", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "The user's coupon was updated", response = void.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "Erreurs rencontrées", response = void.class) })

	public Response userCouponsArchivedUserCouponIdPut(
			@ApiParam(value = "ID of user's coupon", required = true) @PathParam("userCouponId") final String userCouponId,
			@ApiParam(value = "SID (session ID)") @HeaderParam("sid") final String sid,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("date_source_appel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ip_source") final String ipSource,
			@ApiParam(value = "L'id du device") @HeaderParam("id_device") final String idDevice,
			@ApiParam(value = "Latitude du device") @HeaderParam("latitude_device") final String latitudeDevice,
			@ApiParam(value = "Longitude du device") @HeaderParam("longitude_device") final String longitudeDevice,
			@ApiParam(value = "Le type de projection pour la localisation") @HeaderParam("type_projection_localisation") final String typeProjectionLocalisation,
			@ApiParam(value = "L'identifiant de l'application source") @HeaderParam("identifiant_application_source") final String identifiantApplicationSource,
			@ApiParam(value = "Le nom de l'application source") @HeaderParam("nom_application_source") final String nomApplicationSource,
			@ApiParam(value = "La version de l'application source") @HeaderParam("version_application_source") final String versionApplicationSource,
			@ApiParam(value = "L'identifiant de l'utilisateur final") @HeaderParam("identifiant_utilisateur_final") final String identifiantUtilisateurFinal,
			@ApiParam(value = "Le paramètre numéro n, pour le filtrage") @QueryParam("ressources_parametre_n") final String ressourcesParametreN,
			@ApiParam(value = "La date d'une ancienne version de la ressource voulue. Permet de de pointer vers une ressource à une date antérieure. Par défaut si ce paramètre n'est pas spécifié alors ça sera la ressource à la date la plus récente qui sera pointée") @QueryParam("date_ressource_ancienne_version") final String dateRessourceAncienneVersion,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.userCouponsArchivedUserCouponIdPut(userCouponId, sid, dateSourceAppel, ipSource, idDevice,
				latitudeDevice, longitudeDevice, typeProjectionLocalisation, identifiantApplicationSource,
				nomApplicationSource, versionApplicationSource, identifiantUtilisateurFinal, ressourcesParametreN,
				dateRessourceAncienneVersion, securityContext);
	}

	@PUT
	@Path("/clipped/{userCouponId}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "Move user coupon to clipped list.", response = void.class, tags = {
			"user coupon", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "The user's coupon was updated", response = void.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "Erreurs rencontrées", response = void.class) })

	public Response userCouponsClippedUserCouponIdPut(
			@ApiParam(value = "ID of user's coupon", required = true) @PathParam("userCouponId") final String userCouponId,
			@ApiParam(value = "SID (session ID)") @HeaderParam("sid") final String sid,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("date_source_appel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ip_source") final String ipSource,
			@ApiParam(value = "L'id du device") @HeaderParam("id_device") final String idDevice,
			@ApiParam(value = "Latitude du device") @HeaderParam("latitude_device") final String latitudeDevice,
			@ApiParam(value = "Longitude du device") @HeaderParam("longitude_device") final String longitudeDevice,
			@ApiParam(value = "Le type de projection pour la localisation") @HeaderParam("type_projection_localisation") final String typeProjectionLocalisation,
			@ApiParam(value = "L'identifiant de l'application source") @HeaderParam("identifiant_application_source") final String identifiantApplicationSource,
			@ApiParam(value = "Le nom de l'application source") @HeaderParam("nom_application_source") final String nomApplicationSource,
			@ApiParam(value = "La version de l'application source") @HeaderParam("version_application_source") final String versionApplicationSource,
			@ApiParam(value = "L'identifiant de l'utilisateur final") @HeaderParam("identifiant_utilisateur_final") final String identifiantUtilisateurFinal,
			@ApiParam(value = "Le paramètre numéro n, pour le filtrage") @QueryParam("ressources_parametre_n") final String ressourcesParametreN,
			@ApiParam(value = "La date d'une ancienne version de la ressource voulue. Permet de de pointer vers une ressource à une date antérieure. Par défaut si ce paramètre n'est pas spécifié alors ça sera la ressource à la date la plus récente qui sera pointée") @QueryParam("date_ressource_ancienne_version") final String dateRessourceAncienneVersion,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.userCouponsClippedUserCouponIdPut(userCouponId, sid, dateSourceAppel, ipSource, idDevice,
				latitudeDevice, longitudeDevice, typeProjectionLocalisation, identifiantApplicationSource,
				nomApplicationSource, versionApplicationSource, identifiantUtilisateurFinal, ressourcesParametreN,
				dateRessourceAncienneVersion, securityContext);
	}

	@GET
	@Path("/{userCouponId}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "Get a user's coupon", notes = "", response = UserCoupon.class, tags = {
			"coupon" })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "The user's coupon", response = UserCoupon.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "Erreurs rencontrées", response = UserCoupon.class) })

	public Response userCouponsUserCouponIdGet(
			@ApiParam(value = "ID of user's coupon", required = true) @PathParam("userCouponId") final String userCouponId,
			@ApiParam(value = "SID (session ID)") @HeaderParam("sid") final String sid,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("date_source_appel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ip_source") final String ipSource,
			@ApiParam(value = "L'id du device") @HeaderParam("id_device") final String idDevice,
			@ApiParam(value = "Latitude du device") @HeaderParam("latitude_device") final String latitudeDevice,
			@ApiParam(value = "Longitude du device") @HeaderParam("longitude_device") final String longitudeDevice,
			@ApiParam(value = "Le type de projection pour la localisation") @HeaderParam("type_projection_localisation") final String typeProjectionLocalisation,
			@ApiParam(value = "L'identifiant de l'application source") @HeaderParam("identifiant_application_source") final String identifiantApplicationSource,
			@ApiParam(value = "Le nom de l'application source") @HeaderParam("nom_application_source") final String nomApplicationSource,
			@ApiParam(value = "La version de l'application source") @HeaderParam("version_application_source") final String versionApplicationSource,
			@ApiParam(value = "L'identifiant de l'utilisateur final") @HeaderParam("identifiant_utilisateur_final") final String identifiantUtilisateurFinal,
			@ApiParam(value = "Le paramètre numéro n, pour le filtrage") @QueryParam("ressources_parametre_n") final String ressourcesParametreN,
			@ApiParam(value = "La date d'une ancienne version de la ressource voulue. Permet de de pointer vers une ressource à une date antérieure. Par défaut si ce paramètre n'est pas spécifié alors ça sera la ressource à la date la plus récente qui sera pointée") @QueryParam("date_ressource_ancienne_version") final String dateRessourceAncienneVersion,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.userCouponsUserCouponIdGet(userCouponId, sid, dateSourceAppel, ipSource, idDevice,
				latitudeDevice, longitudeDevice, typeProjectionLocalisation, identifiantApplicationSource,
				nomApplicationSource, versionApplicationSource, identifiantUtilisateurFinal, ressourcesParametreN,
				dateRessourceAncienneVersion, securityContext);
	}
}
