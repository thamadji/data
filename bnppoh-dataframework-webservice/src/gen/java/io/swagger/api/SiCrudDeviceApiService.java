package io.swagger.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.model.Device;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-02-02T10:10:43.385Z")
public abstract class SiCrudDeviceApiService {

	public abstract Response siCrudDevicePost(Device body, String dateSourceAppel, SecurityContext securityContext)
			throws NotFoundException;

}
