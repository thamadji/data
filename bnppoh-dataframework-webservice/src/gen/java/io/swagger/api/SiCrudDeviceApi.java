package io.swagger.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.annotations.ApiParam;
import io.swagger.api.factories.SiCrudDeviceApiServiceFactory;
import io.swagger.model.Device;

@Path("/devices")
@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
@io.swagger.annotations.Api(description = "the si_crud_device API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-02-02T10:10:43.385Z")
public class SiCrudDeviceApi {
	private final SiCrudDeviceApiService delegate = SiCrudDeviceApiServiceFactory.getSiCrudDeviceApi();

	@POST

	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "Creates a new device", response = Void.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {})

	public Response siCrudDevicePost(
			@ApiParam(value = "The object to be saved in the database", required = true) final Device body,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudDevicePost(body, dateSourceAppel, securityContext);
	}
}
