package io.swagger.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.annotations.ApiParam;
import io.swagger.api.factories.SiCrudIndividualContactsApiServiceFactory;
import io.swagger.jaxrs.PATCH;
import io.swagger.model.IndividualContact;

@Path("/individual_contacts")
@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
@io.swagger.annotations.Api(description = "the si_crud_individual_contacts API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")

public class SiCrudIndividualContactsApi {
	private final SiCrudIndividualContactsApiService delegate = SiCrudIndividualContactsApiServiceFactory
			.getSiCrudIndividualContactsApi();

	// @Consumes({ "application/json", "application/xml", "text/xml",
	// "text/html" })
	@GET
	@Produces("application/json")
	@io.swagger.annotations.ApiOperation(value = "", notes = "Returns all individual contacts from the data platform MarkLogic", response = IndividualContact.class, responseContainer = "List", tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "individual contact response", response = IndividualContact.class, responseContainer = "List"),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = IndividualContact.class, responseContainer = "List") })

	public Response siCrudIndividualContactsGet(
			@ApiParam(value = "data to filter by timestamp", required = true) @HeaderParam("timestamp") final List<String> timestamp,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@ApiParam(value = "tags to filter by") @QueryParam("firstName") final List<String> firstName,
			@ApiParam(value = "tags to filter by") @QueryParam("lastName") final List<String> lastName,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsGet(timestamp, dateSourceAppel, ipSource, firstName, lastName,
				securityContext);
	}

	@POST
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces("application/json")
	@io.swagger.annotations.ApiOperation(value = "", notes = "Creates a new individual contact in the system", tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 201, message = "Individual Contact response", response = IndividualContact.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = IndividualContact.class) })

	public Response siCrudIndividualContactsPost(
			@ApiParam(value = "The object to be saved in the database", required = true) final IndividualContact body,
			@HeaderParam("deviceId") final String deviceId,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsPost(body, deviceId, dateSourceAppel, ipSource, securityContext);
	}

	@GET
	@Path("/{id}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "Returns an individual contact based on a single ID", response = IndividualContact.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "individual contact response", response = IndividualContact.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = IndividualContact.class) })

	public Response siCrudIndividualContactsIdGet(
			@ApiParam(value = "ID of individual contact to fetch", required = true) @PathParam("id") final String id,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsIdGet(id, dateSourceAppel, ipSource, securityContext);
	}

	@PUT
	@Path("/{id}")
	// @Consumes({ "application/json", "application/xml", "text/xml",
	// "text/html" })
	@Produces("application/json")
	@io.swagger.annotations.ApiOperation(value = "", notes = "update an individual contact based on a single ID", response = IndividualContact.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "individual contact response", response = IndividualContact.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = IndividualContact.class) })

	public Response siCrudIndividualContactsIdPut(
			@ApiParam(value = "ID of individual contact to fetch", required = true) @PathParam("id") final String id,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@ApiParam(value = "Parameters to update for individuals contacts") final IndividualContact body,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsIdPut(id, dateSourceAppel, ipSource, body, securityContext);
	}

	@DELETE
	@Path("/{id}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "deletes a single individual contact based on the ID supplied", response = Void.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 204, message = "individual contact deleted", response = Void.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = Void.class) })

	public Response siCrudIndividualContactsIdDelete(
			@ApiParam(value = "ID of individual contact to delete", required = true) @PathParam("id") final String id,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsIdDelete(id, dateSourceAppel, ipSource, securityContext);
	}

	@PATCH
	@Path("/{id}")
	@Consumes({ "application/json", "application/xml", "text/xml", "text/html" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@io.swagger.annotations.ApiOperation(value = "", notes = "update partial an individual contact", response = IndividualContact.class, tags = {})
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "individual contact response", response = IndividualContact.class),

			@io.swagger.annotations.ApiResponse(code = 200, message = "unexpected error", response = IndividualContact.class) })

	public Response siCrudIndividualContactsIdPatch(
			@ApiParam(value = "ID of individual contact to fetch", required = true) @PathParam("id") final String id,
			@HeaderParam("deviceId") final String deviceId,
			@ApiParam(value = "La date d'appel de la source") @HeaderParam("dateSourceAppel") final String dateSourceAppel,
			@ApiParam(value = "L'ip de la source") @HeaderParam("ipSource") final String ipSource,
			@ApiParam(value = "Parameters to update for individuals contacts") final IndividualContact body,
			@Context final SecurityContext securityContext) throws NotFoundException {
		return delegate.siCrudIndividualContactsIdPatch(id, deviceId, dateSourceAppel, ipSource, body, securityContext);
	}
}
