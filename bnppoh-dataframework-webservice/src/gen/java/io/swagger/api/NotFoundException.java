package io.swagger.api;

@SuppressWarnings("serial")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public class NotFoundException extends ApiException {
	private final int code;

	public NotFoundException(final int _code, final String msg) {
		super(_code, msg);
		this.code = _code;
	}
}
