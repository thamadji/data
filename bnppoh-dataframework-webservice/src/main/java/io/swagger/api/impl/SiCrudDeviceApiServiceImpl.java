package io.swagger.api.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.DeviceVO;
import com.bnp.services.DeviceServiceImpl;
import com.bnp.services.IDeviceService;
import com.bnp.tools.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.api.NotFoundException;
import io.swagger.api.SiCrudDeviceApiService;
import io.swagger.model.Device;

/**
 *
 * @author thamadji
 *
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-02-02T10:10:43.385Z")
public class SiCrudDeviceApiServiceImpl extends SiCrudDeviceApiService {
	/**
	 * Path du webservice
	 */
	public final static String WEBSERVICE_PATH = "/";
	private static final Logger LOGGER = Logger.getLogger(SiCrudDeviceApiServiceImpl.class);

	@Override
	public Response siCrudDevicePost(final Device body, final String dateSourceAppel,
			final SecurityContext securityContext) throws NotFoundException {
		LOGGER.debug("Entree dans le post device");
		DeviceVO vDevice = new DeviceVO();
		final IDeviceService vDeviceService = new DeviceServiceImpl();
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		String vDeviceJson = "";
		String postResult = "";
		URI location = null;
		Date vValidStart = null;

		try {
			vDeviceJson = objectMapper.writeValueAsString(body);
			LOGGER.debug("JSON Device =" + vDeviceJson);
			vDevice = objectMapper.readValue(vDeviceJson, DeviceVO.class);
			if (dateSourceAppel != null) {
				vValidStart = new Date();

				vValidStart = Utils.parseDate(dateSourceAppel);
				LOGGER.debug("Timestamp apr�s parsing");

			}

			postResult = vDeviceService.addDevice(vDevice, vValidStart);

			location = new URI(WEBSERVICE_PATH + postResult);

		} catch (final JsonProcessingException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final IOException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final URISyntaxException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final ParseException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.status(201).header("Location", location).build();
	}

}
