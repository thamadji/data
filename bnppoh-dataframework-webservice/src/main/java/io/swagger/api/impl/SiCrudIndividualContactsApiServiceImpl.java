package io.swagger.api.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.services.IIndividualContactService;
import com.bnp.services.IndividualContactServiceImpl;
import com.bnp.tools.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.SiCrudIndividualContactsApiService;
import io.swagger.model.IndividualContact;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public class SiCrudIndividualContactsApiServiceImpl extends SiCrudIndividualContactsApiService {

	/**
	 * Declaration des codes retour utilis�s
	 */

	/**
	 * Renvoyé lors que la creation s'est bien effectu�e
	 */
	public final static int CREATED = 201, UPDATED = 201;

	/**
	 * Renvoy� lorsque le contenu demand� est bien retourn�
	 */
	public final static int OK = 200;

	/**
	 * Path du webservice
	 */
	public final static String WEBSERVICE_PATH = "/";

	final static Logger LOGGER = Logger.getLogger(SiCrudIndividualContactsApiServiceImpl.class);

	/**
	 * Methode GET By ID
	 */
	@Override
	public Response siCrudIndividualContactsIdGet(final String id, final String dateSourceAppel, final String ipSource,
			final SecurityContext securityContext) throws NotFoundException {

		LOGGER.debug("Entree dans la methode GET by ID du WebService");
		LOGGER.debug("Date de l'appel " + dateSourceAppel);
		final IIndividualContactService vIndividualContactService = new IndividualContactServiceImpl();
		com.bnp.model.IndividualContactVO vIndividualContactVO = new com.bnp.model.IndividualContactVO();

		final ObjectMapper objectMapper = new ObjectMapper();
		IndividualContact individualContact = new IndividualContact();
		String pResultJson = "";

		try {
			vIndividualContactVO = vIndividualContactService.getIndividualContactById(id);

			LOGGER.debug(vIndividualContactVO);
			pResultJson = objectMapper.writeValueAsString(vIndividualContactVO);
			LOGGER.debug("Contenu renvoye format json = " + pResultJson);
			final String pResult = pResultJson;
			individualContact = objectMapper.readValue(pResult, IndividualContact.class);
			LOGGER.debug("Contenu swagger renvoy� : " + individualContact.toString());

		} catch (final JsonProcessingException e1) {
			e1.printStackTrace();
			return Response.noContent().build();
		} catch (final IOException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.ok(individualContact).build();
	}

	@Override
	public Response siCrudIndividualContactsPost(final IndividualContact body, final String deviceId,
			final String dateSourceAppel, final String ipSource, final SecurityContext securityContext)
					throws NotFoundException {

		final IIndividualContactService individualContactService = new IndividualContactServiceImpl();
		com.bnp.model.IndividualContactVO vIndividualContact = new com.bnp.model.IndividualContactVO();

		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		String jsonStringIndividualContact = "";
		URI location = null;
		String postResult = "";
		Date vValidStart = null;

		try {
			jsonStringIndividualContact = objectMapper.writeValueAsString(body);
			LOGGER.debug("JSON = " + jsonStringIndividualContact);
			vIndividualContact = objectMapper.readValue(jsonStringIndividualContact,
					com.bnp.model.IndividualContactVO.class);
			LOGGER.debug("Pojo individualcontactvo = " + vIndividualContact.toString());

			if (dateSourceAppel != null) {
				vValidStart = Utils.parseDate(dateSourceAppel);
			}
			postResult = individualContactService.addIndividualContact(vIndividualContact, deviceId, vValidStart);

			location = new URI(WEBSERVICE_PATH + postResult);
			LOGGER.debug("location � :" + location);
			System.out.println("location" + location);

		} catch (final JsonProcessingException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final IOException e1) {
			e1.printStackTrace();
			return Response.noContent().build();
		} catch (final URISyntaxException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.status(CREATED).header("Location", location).build();
	}

	@Override
	public Response siCrudIndividualContactsGet(final List<String> timestamp, final String dateSourceAppel,
			final String ipSource, final List<String> firstName, final List<String> lastName,
			final SecurityContext securityContext) throws NotFoundException {

		return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
	}

	@Override
	public Response siCrudIndividualContactsIdPut(final String id, final String dateSourceAppel, final String ipSource,
			final IndividualContact body, final SecurityContext securityContext) throws NotFoundException {
		// do some magic!
		return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
	}

	@Override
	public Response siCrudIndividualContactsIdDelete(final String id, final String dateSourceAppel,
			final String ipSource, final SecurityContext securityContext) throws NotFoundException {
		// do some magic!
		return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
	}

	@Override
	public Response siCrudIndividualContactsIdPatch(final String id, final String deviceId,
			final String dateSourceAppel, final String ipSource, final IndividualContact body,
			final SecurityContext securityContext) throws NotFoundException {

		LOGGER.debug("Entree dans la methode PATCH");
		LOGGER.debug("ID pass� en param = " + id);

		final IIndividualContactService vIndividualContactService = new IndividualContactServiceImpl();
		com.bnp.model.IndividualContactVO vIndividualContact = new com.bnp.model.IndividualContactVO();

		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		String jsonStringIndividualContact = "";
		URI location = null;
		String postResult = "";
		Date vValidStart = null;
		try {
			jsonStringIndividualContact = objectMapper.writeValueAsString(body);

			LOGGER.debug("JSON = " + jsonStringIndividualContact);
			vIndividualContact = objectMapper.readValue(jsonStringIndividualContact,
					com.bnp.model.IndividualContactVO.class);
			LOGGER.debug("Pojo individualcontactvo = " + vIndividualContact.toString());

			if (dateSourceAppel != null) {

				vValidStart = null;
				vValidStart = Utils.parseDate(dateSourceAppel);

				LOGGER.debug("Date apr�s parsage = " + vValidStart.toString());
			}

			postResult = vIndividualContactService.updateIndividualContact(id, deviceId, vIndividualContact,
					vValidStart);

//			location = new URI(WEBSERVICE_PATH + postResult);
			location = new URI("");
			LOGGER.debug("location :" + location);

			System.out.println("location" + location);
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final IOException e) {
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final URISyntaxException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}
		return Response.status(UPDATED).header("Location", location).build();
	}

}