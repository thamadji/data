package io.swagger.api.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.CouponVO;
import com.bnp.services.CouponsServiceImpl;
import com.bnp.services.ICouponsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.api.CouponsApiService;
import io.swagger.api.NotFoundException;
import io.swagger.model.Coupon;
import io.swagger.model.Location;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T14:15:18.428Z")
public class CouponsApiServiceImpl extends CouponsApiService {
	/**
	 * D�claration des codes retour utilis�s
	 */

	/**
	 * Renvoy� lors que la cr�ation s'est bien effectu�e
	 */
	public final static int CREATED = 201, UPDATED = 201;

	/**
	 * Renvoy� lorsque le contenu demand� est bien retourn�
	 */
	public final static int OK = 200;

	/**
	 * Path du webservice
	 */
	public final static String WEBSERVICE_PATH = "/";

	final static Logger LOGGER = Logger.getLogger(CouponsApiServiceImpl.class);

	/**
	 * Methode GET By ID
	 */
	@Override

	public Response couponsCouponIdGet(final String couponId, final String sid, final String dateSourceAppel,
			final String ipSource, final String idDevice, final String latitudeDevice, final String longitudeDevice,
			final String typeProjectionLocalisation, final String identifiantApplicationSource,
			final String nomApplicationSource, final String versionApplicationSource,
			final String identifiantUtilisateurFinal, final String ressourcesParametreN,
			final String dateRessourceAncienneVersion, final SecurityContext securityContext) throws NotFoundException {

		LOGGER.debug("Entree dans la methode GET by ID du WebService");
		LOGGER.debug("Date de l'appel " + dateSourceAppel);
		final ICouponsService vICouponsService = new CouponsServiceImpl();
		CouponVO vCouponVO = new CouponVO();

		final Coupon vResultCoupon = new Coupon();
		final List<String> list = new ArrayList<String>();

		final ObjectMapper objectMapper = new ObjectMapper();
		// final CouponVO pCouponVO = new CouponVO();
		String pResultJson = "";
		try {

			vCouponVO = vICouponsService.getCoupon(couponId);
			list.add("http://www.astae.fr/images/labnovation/lingerie.png"); //list.add(vCouponVO.getImage());
			// Mapping
			vResultCoupon.setId(vCouponVO.getCouponId());
			//vResultCoupon.setDescription(vCouponVO.getDescription());
			vResultCoupon.setDescription("Meet our most intuitive sport bra, with maximum support and wireless comfort, plus a cushioned abck to blast through floor exercises and adjustable straps and band for a custom fit.");
			vResultCoupon.setTitle("Ensemble Lingerie Standout");
			vResultCoupon.setPrice("50 �");
			vResultCoupon.setDiscountPercentage("50%");
			vResultCoupon.setDiscountedPrice("25 �");
			vResultCoupon.setDiscountConditions("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec semper lacus eget est aliquam laoreet. Duis sit amet congue eros, vitae porttitor erat. Ut sem elit, consequat scelerisque eros quis, mattis ultricies purus. Duis euismod ante odio, ac congue urna elementum a. Quisque vel arcu semper, venenatis risus at, condimentum ante. Aliquam fringilla, sapien at auctor lobortis, nulla nisl cursus felis, non aliquam nunc libero ac leo. Maecenas id posuere erat, eget lacinia lorem. Curabitur sit amet vulputate urna. Aenean eu feugiat nisi, at aliquam quam. ");
			vResultCoupon.setExpiryDate("2017-01-01 00:00:00");
			vResultCoupon.setImageURIs(list);
			vResultCoupon.setLocation(new Location());
			vResultCoupon.getLocation().setId("31");
			vResultCoupon.getLocation().setName("Paris");
			vResultCoupon.getLocation().setAddress("Address");
			vResultCoupon.getLocation().setLatitude(48.906291);
			vResultCoupon.getLocation().setLongitude(2.040222);
			vResultCoupon.getLocation().setZipCode("75000");
			vResultCoupon.getLocation().setWebsite("www.Marklogic.com");
			vResultCoupon.getLocation().setPhoneNumber("0666226622");
			vResultCoupon.getLocation().setOpeningSchedule("Schedules");

			LOGGER.debug(vResultCoupon);
			pResultJson = objectMapper.writeValueAsString(vResultCoupon);
			LOGGER.debug("Contenu renvoyé format json = " + pResultJson);
			// String pResult = pResultJson;
			// pCouponVO = objectMapper.readValue(pResult, CouponVO.class);
			LOGGER.debug("Contenu swagger renvoyé : " + vResultCoupon.toString());

		} catch (final JsonProcessingException e1) {
			e1.printStackTrace();
			return Response.noContent().build();

		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.ok(pResultJson).build();
	}

}
