package io.swagger.api.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.UserCouponVO;
import com.bnp.services.IUserCouponService;
import com.bnp.services.UserCouponServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.api.NotFoundException;
import io.swagger.api.UserCouponsApiService;
import io.swagger.model.UserCoupon;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T14:15:18.428Z")
public class UserCouponsApiServiceImpl extends UserCouponsApiService {

	/**
	 * D�claration des codes retour utilis�s
	 */

	/**
	 * Renvoy� lors que la cr�ation s'est bien effectu�e
	 */
	public final static int CREATED = 201, UPDATED = 201;

	/**
	 * Renvoy� lorsque le contenu demand� est bien retourn�
	 */
	public final static int OK = 200;

	/**
	 * Path du webservice
	 */
	public final static String WEBSERVICE_PATH = "/";

	final static Logger LOGGER = Logger.getLogger(UserCouponsApiServiceImpl.class);

	/**
	 * Methode GET By ID
	 */

	@Override
	public Response userCouponsPost(final UserCoupon body, final String dateSourceAppel, final String deviceId,
			final String ipSource, final SecurityContext securityContext) throws NotFoundException {
		final IUserCouponService vUserCouponService = new UserCouponServiceImpl();
		UserCouponVO vUserCouponVO = new UserCouponVO();

		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		String jsonStringUserCouponVO = "";
		URI location = null;
		String postResult = "";
		Date vValidStart = null;

		try {
			//jsonStringUserCouponVO = objectMapper.writeValueAsString(body);
			LOGGER.debug("JSON = " + jsonStringUserCouponVO);
			//vUserCouponVO = objectMapper.readValue(jsonStringUserCouponVO, com.bnp.model.coupon.UserCouponVO.class);
			vUserCouponVO.setCouponId(body.getCouponId());
			vUserCouponVO.setIndividualContactId(body.getIndividualContactId());
			LOGGER.debug("Pojo UserCouponVO = " + vUserCouponVO.toString());

			if (dateSourceAppel != null) {
				final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.FRANCE);
				vValidStart = new Date();
				try {
					vValidStart = formatter.parse(dateSourceAppel);
					LOGGER.debug("Date apr�s parsing =" + formatter.format(vValidStart));
				} catch (final ParseException e) {
					vValidStart = new Date();
				}
			}

			postResult = vUserCouponService.addUserCoupon(vUserCouponVO);

			location = new URI(WEBSERVICE_PATH + postResult);
			LOGGER.debug("location � :" + location);
			System.out.println("location" + location);

		}   catch (final TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.noContent().build();
		} catch (final URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.noContent().build();
		}

		// return Response.ok(vUserCouponVO.getUserCouponId()).build();
		return Response.status(CREATED).header("location", location).build();
	}

	@Override
	public Response userCouponsArchivedUserCouponIdPut(final String userCouponId, final String sid,
			final String dateSourceAppel, final String ipSource, final String idDevice, final String latitudeDevice,
			final String longitudeDevice, final String typeProjectionLocalisation,
			final String identifiantApplicationSource, final String nomApplicationSource,
			final String versionApplicationSource, final String identifiantUtilisateurFinal,
			final String ressourcesParametreN, final String dateRessourceAncienneVersion,
			final SecurityContext securityContext) throws NotFoundException {
		LOGGER.debug("Entree dans la methode userCouponsArchivedUserCouponIdPut PUT du WebService");
		final IUserCouponService vUserCouponService = new UserCouponServiceImpl();
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		try {

			vUserCouponService.archiverUserCoupon(userCouponId);

		} catch (final TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.status(OK).build();
	}

	@Override
	public Response userCouponsClippedUserCouponIdPut(final String userCouponId, final String sid,
			final String dateSourceAppel, final String ipSource, final String idDevice, final String latitudeDevice,
			final String longitudeDevice, final String typeProjectionLocalisation,
			final String identifiantApplicationSource, final String nomApplicationSource,
			final String versionApplicationSource, final String identifiantUtilisateurFinal,
			final String ressourcesParametreN, final String dateRessourceAncienneVersion,
			final SecurityContext securityContext) throws NotFoundException {
		LOGGER.debug("Entree dans la methode userCouponsClippedUserCouponIdPut PUT du WebService");
		final IUserCouponService vUserCouponService = new UserCouponServiceImpl();
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		try {

			vUserCouponService.clipperUserCoupon(userCouponId);

		} catch (final TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.status(OK).build();
	}

	@Override
	public Response userCouponsUserCouponIdGet(final String userCouponId, final String sid,
			final String dateSourceAppel, final String ipSource, final String idDevice, final String latitudeDevice,
			final String longitudeDevice, final String typeProjectionLocalisation,
			final String identifiantApplicationSource, final String nomApplicationSource,
			final String versionApplicationSource, final String identifiantUtilisateurFinal,
			final String ressourcesParametreN, final String dateRessourceAncienneVersion,
			final SecurityContext securityContext) throws NotFoundException {
		LOGGER.debug("Entree dans la methode GET by ID du WebService");
		LOGGER.debug("Date de l'appel " + dateSourceAppel);
		final IUserCouponService vUserCouponService = new UserCouponServiceImpl();
		UserCouponVO vUserCouponVO = new UserCouponVO();

		final UserCoupon pUserCoupon = new UserCoupon();

		final ObjectMapper objectMapper = new ObjectMapper();
		final UserCouponVO pUserCouponVO = new UserCouponVO();
		String pResultJson = "";
		try {
			vUserCouponVO = vUserCouponService.getUserCoupon(userCouponId);

			// Mapping
			pUserCoupon.setCouponId(vUserCouponVO.getCouponId());
			pUserCoupon.setIndividualContactId(vUserCouponVO.getIndividualContactId());
			pUserCoupon.setStatus(vUserCouponVO.getTriples().getTripleList().get(0).getObject().replaceAll("/userCouponState/", ""));
			
			LOGGER.debug(pUserCoupon);
			pResultJson = objectMapper.writeValueAsString(pUserCoupon);
			LOGGER.debug("Contenu renvoye format json = " + pResultJson);

			LOGGER.debug("Contenu swagger renvoy� : " + pUserCouponVO.toString());

		} catch (final JsonProcessingException e1) {
			e1.printStackTrace();
			return Response.noContent().build();
		} catch (final TechnicalException e) {
			e.printStackTrace();
			return Response.noContent().build();
		}

		return Response.ok(pResultJson).build();
	}

}
