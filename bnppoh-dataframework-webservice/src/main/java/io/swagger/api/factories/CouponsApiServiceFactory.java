package io.swagger.api.factories;

import io.swagger.api.CouponsApiService;
import io.swagger.api.impl.CouponsApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public class CouponsApiServiceFactory {

	private static final CouponsApiService SERVICE = new CouponsApiServiceImpl();

	public static CouponsApiService getCouponsApi() {
		return SERVICE;
	}
}
