package io.swagger.api.factories;

import io.swagger.api.UserCouponsApiService;
import io.swagger.api.impl.UserCouponsApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-02-08T15:04:46.798Z")
public class UserCouponsApiServiceFactory {

	private static final UserCouponsApiService SERVICE = new UserCouponsApiServiceImpl();

	public static UserCouponsApiService getUserCouponsApi() {
		return SERVICE;
	}
}
