package io.swagger.api.factories;

import io.swagger.api.SiCrudDeviceApiService;
import io.swagger.api.impl.SiCrudDeviceApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-02-02T10:10:43.385Z")
public class SiCrudDeviceApiServiceFactory {

	private static final SiCrudDeviceApiService SERVICE = new SiCrudDeviceApiServiceImpl();

	public static SiCrudDeviceApiService getSiCrudDeviceApi() {
		return SERVICE;
	}
}
