package io.swagger.api.factories;

import io.swagger.api.SiCrudIndividualContactsApiService;
import io.swagger.api.impl.SiCrudIndividualContactsApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T17:03:36.408Z")
public class SiCrudIndividualContactsApiServiceFactory {

	private static final SiCrudIndividualContactsApiService SERVICE = new SiCrudIndividualContactsApiServiceImpl();

	public static SiCrudIndividualContactsApiService getSiCrudIndividualContactsApi() {
		return SERVICE;
	}
}
