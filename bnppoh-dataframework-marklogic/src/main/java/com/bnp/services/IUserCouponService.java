package com.bnp.services;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.UserCouponVO;

/**
 * 
 * @author bdansoko
 *
 */
public interface IUserCouponService {

	/**
	 * 
	 * @param pCouponId
	 * @param pIndividualContactId
	 * @return
	 * @throws TechnicalException
	 */
	public String addUserCoupon(UserCouponVO pUserCouponVO) throws TechnicalException;
	
	/**
	 * 
	 * @param pUserCouponID
	 * @return
	 * @throws TechnicalException
	 */
	public UserCouponVO getUserCoupon (String pUserCouponID) throws TechnicalException;
	
	/**
	 * 
	 * @param pUserCouponID
	 * @throws TechnicalException
	 */
	public void archiverUserCoupon (String pUserCouponID) throws TechnicalException;
	
	/**
	 * 
	 * @param pUserCouponID
	 * @throws TechnicalException
	 */
	public void clipperUserCoupon (String pUserCouponID) throws TechnicalException;	
}
