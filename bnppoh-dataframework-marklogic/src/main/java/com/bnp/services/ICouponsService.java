package com.bnp.services;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.CouponVO;

/**
 * 
 * @author bdansoko
 *
 */
public interface ICouponsService {

	/**
	 * 
	 * @param pCouponsVO
	 * @return
	 * @throws TechnicalException
	 */
	public String addCoupon(CouponVO pCouponsVO) throws TechnicalException;
	
	/**
	 * 
	 * @param pCouponsVOID
	 * @return
	 */
	public CouponVO getCoupon (String pCouponID) throws TechnicalException;
}
