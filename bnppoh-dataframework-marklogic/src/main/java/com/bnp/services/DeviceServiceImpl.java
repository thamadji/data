package com.bnp.services;

import java.text.ParseException;
import java.util.Date;
import org.apache.log4j.Logger;

import com.bnp.dao.IDeviceDao;
import com.bnp.dao.IIndividualContactEventDao;
import com.bnp.dao.impl.DeviceDaoImpl;
import com.bnp.dao.impl.IndividualContactEventDaoImpl;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.DeviceVO;
import com.bnp.model.MarkLogicVO;
import com.bnp.model.PointOfSalesVO;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.tools.Configuration;
import com.bnp.tools.Constantes;
import com.bnp.tools.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.eval.ServerEvaluationCall;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.semantics.SPARQLMimeTypes;
import com.marklogic.client.semantics.SPARQLQueryDefinition;
import com.marklogic.client.semantics.SPARQLQueryManager;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 *
 * @author thamadji
 *
 */
public class DeviceServiceImpl implements IDeviceService {

	private static final Logger LOGGER = Logger.getLogger(DeviceServiceImpl.class);

	@Override
	public String addDevice(final DeviceVO pDevice, final Date pTimeStamp) throws TechnicalException, ParseException {

		final IDeviceDao vDeviceDao = new DeviceDaoImpl();
		final DeviceVO vDevice = new DeviceVO();
		String postResult;
		String vDeviceUri = Constantes.DEVICE_URI_PREFIX+pDevice.getDeviceId();
		String vReverseQueryResponse;
		PointOfSalesVO vPointOfSale = null;

		vDevice.setDeviceId(pDevice.getDeviceId());
		final Date vValidEnd = Utils.infinitedate;
		LOGGER.debug("ValidEndDate = " + vValidEnd);

		if (pTimeStamp != null) {
			vDevice.setValidStart(pTimeStamp);
		}

		vDevice.setValidEnd(vValidEnd);

		if (pDevice.getLocalization() != null) {
			vDevice.setLocalization(pDevice.getLocalization());
		}
		 postResult = vDeviceDao.addDevice(vDevice);
		// On recupere un seul magasin
		vReverseQueryResponse = reverseQuerySearch(vDeviceUri);
		if (vReverseQueryResponse != null) {
			// Mapping PointOfSaleVO
			final XStream xstream = new XStream(new StaxDriver());
			xstream.processAnnotations(MarkLogicVO.class);
			xstream.processAnnotations(TriplesVO.class);
			xstream.processAnnotations(TripleVO.class);
			xstream.processAnnotations(PointOfSalesVO.class);
			
			vPointOfSale = (PointOfSalesVO) xstream.fromXML(vReverseQueryResponse);
			LOGGER.debug("Point of sale posifif en POJO : \n"+vPointOfSale);
			
			// Recherche du contact individuel à partir de l'ID device
			String individualContacId = getUserIdByDeviceUri(vDeviceUri);
			LOGGER.debug("Id contact individuel à envoyer au service = "+individualContacId);
//			System.setProperty("javax.net.debug", "ssl");
			IIndividualContactEventDao dao =  new IndividualContactEventDaoImpl();
			dao.sendIndividualContactInPointOfSales(individualContacId, vPointOfSale.getPointOfSalesID(), vDevice.getDeviceId());
		} else {
			LOGGER.warn("Le resultat de la reverse query est negatif");
		}

		return pDevice.getDeviceId();
	}

	private String reverseQuerySearch(final String pDeviceUri) {

		final DatabaseClient client = DatabaseClientFactory.newClient(Configuration.DB_HOST, Configuration.DB_PORT,
				Configuration.DB_USER_NAME, Configuration.DB_PASSWORD, Authentication.DIGEST);
		final ServerEvaluationCall theCall = client.newServerEval();

		final String query = "xquery version \"1.0-ml\";"
				+ "import module namespace temporal = \"http://marklogic.com/xdmp/temporal\" at \"/MarkLogic/temporal.xqy\";"
				+ "let $me := doc(\"" + pDeviceUri + "\")/" + Constantes.DEVICE_COLLECTION + " "
				+ "for $match in cts:search(/" + Constantes.POINT_OF_SALES_COLLECTION + "," + "cts:and-query(("
				+ "cts:collection-query((\"latest\"))," + "cts:reverse-query($me)" + "))" + ")" + "return  $match";
		theCall.xquery(query);

		LOGGER.debug("Contenu de la query pour activer la reverse query : \n " + query);
		final String response = theCall.evalAs(String.class);
		LOGGER.debug("Contenu de la reponse renvoyee = " + response);

		client.release();

		return response;
	}
	private String getUserIdByDeviceUri(String pDeviceUri) {

		String vUserId = "";

		final DatabaseClient client = DatabaseClientFactory.newClient(Configuration.DB_HOST, Configuration.DB_PORT,
				Configuration.DB_USER_NAME, Configuration.DB_PASSWORD, Authentication.DIGEST);

		final SPARQLQueryManager sqmgr = client.newSPARQLQueryManager();

		final SPARQLQueryDefinition query = sqmgr.newQueryDefinition("SELECT ?s WHERE { ?s ?p ?o} ")
				.withBinding("p", Constantes.HAS_DEVICE).withBinding("o", pDeviceUri);
		final JacksonHandle handle = new JacksonHandle();
		handle.setMimetype(SPARQLMimeTypes.SPARQL_JSON);
		JacksonHandle results = sqmgr.executeSelect(query, handle);
		
		LOGGER.debug("Contenu du jackson result : "+results);
		JsonNode tuples = results.get().path("results").path("bindings");
		for ( JsonNode row : tuples ) {
		    String s = row.path("s").path("value").asText();
		    LOGGER.debug("URI du contact Individuel ayant le Device :"+pDeviceUri +" est : "+s);
		    vUserId = s.replace(Constantes.INDIVIDUAL_CONTACT_URI_PREFIX, "");

		}

		return vUserId;
	}
}
