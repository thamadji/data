package com.bnp.services;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.PointOfSalesVO;

/**
 * A completer
 * @author jnairain
 *
 */
public interface IPointOfSalesService {

	/**
	 * A completer
	 * @param pPointOfSalesVO
	 * @return
	 */
	public String addPointOfSales(PointOfSalesVO pPointOfSalesVO) throws TechnicalException;
	
	/**
	 * A completer
	 * @param pPointOfSalesVOID
	 * @return
	 */
	public PointOfSalesVO getPointOfSales (String pPointOfSalesVOID) throws TechnicalException;
}
