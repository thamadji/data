package com.bnp.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.bnp.dao.IIndividualContactDao;
import com.bnp.dao.INotificationKafkaDao;
import com.bnp.dao.impl.IndividualContactDaoImpl;
import com.bnp.dao.impl.NotificationKafkaDaoImpl;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.tools.Constantes;
import com.bnp.tools.Utils;

public class IndividualContactServiceImpl implements IIndividualContactService {

	private static final Logger LOGGER = Logger.getLogger(IndividualContactServiceImpl.class);

	final IIndividualContactDao mIndividualContactDao = new IndividualContactDaoImpl();

	@Override
	public IndividualContactVO getIndividualContactById(final String id) throws TechnicalException {

		final String vDocumentUri = Constantes.INDIVIDUAL_CONTACT_URI_PREFIX + id;

		final IndividualContactVO vIndividualContact = mIndividualContactDao.getIndividualContactById(vDocumentUri);
		return vIndividualContact;
	}

	@Override
	public String addIndividualContact(final IndividualContactVO pIndividualContact, final String deviceId,
			final Date pDate) throws TechnicalException {

		LOGGER.debug("POST : Valeur de l'id = " + pIndividualContact.getId());

		if (null == pIndividualContact.getId() || pIndividualContact.getId().equals("")) {

			pIndividualContact.setId(Utils.createUniqueId());

			final String vIndividualContactURI = Constantes.INDIVIDUAL_CONTACT_URI_PREFIX + pIndividualContact.getId();
			if (pDate != null) {
				pIndividualContact.setValidStart(pDate);
			}
			pIndividualContact.setValidEnd(Utils.infinitedate);

			if (deviceId != null) {
				final String vDeviceURI = Constantes.DEVICE_URI_PREFIX + deviceId;
				final TripleVO vTriple = new TripleVO(vIndividualContactURI, Constantes.HAS_DEVICE, vDeviceURI);
				final List<TripleVO> tripleList = new ArrayList<TripleVO>();
				tripleList.add(vTriple);
				final TriplesVO vTriples = new TriplesVO(tripleList);

				pIndividualContact.setTriples(vTriples);
			} else {
				LOGGER.warn("Paramaetre deviceId non envoyé ou nul");
			}

			// Dao
			final String vDocumentURI = mIndividualContactDao.addIndividualContact(pIndividualContact);
			LOGGER.debug("URI du document renvoyé = " + vDocumentURI);
			LOGGER.debug("ID recupere à partir de l'URI = " + pIndividualContact.getId());

			// Notification
			final INotificationKafkaDao vNotificationKafkaDao = new NotificationKafkaDaoImpl();

			vNotificationKafkaDao.sendMessage(Utils.messageToSend(pIndividualContact));
			return pIndividualContact.getId();

		} else {
			throw new TechnicalException(
					"Le contact individuel passé en parametre a déjà un ID. Il faut faire un PATCH plutôt qu'un POST");
		}

	}

	@Override
	public String updateIndividualContact(final String pIndividualContactId, final String pDeviceId,
			final IndividualContactVO pIndividualContact, final Date pDate) throws TechnicalException {
		IndividualContactVO vIndividualContact = null;

		final String vDocumentURI = Constantes.INDIVIDUAL_CONTACT_URI_PREFIX + pIndividualContactId;

		// Dao
		vIndividualContact = mIndividualContactDao.getIndividualContactById(vDocumentURI);

		if (pDate != null) {
			vIndividualContact.setValidStart(pDate);
		}
		vIndividualContact.setValidEnd(Utils.infinitedate);

		if (null != pIndividualContact.getFirstName()) {
			vIndividualContact.setFirstName(pIndividualContact.getFirstName());
		}
		if (null != pIndividualContact.getLastName()) {
			vIndividualContact.setLastName(pIndividualContact.getLastName());
		}
		if (null != pIndividualContact.getEmail()) {
			vIndividualContact.setEmail(pIndividualContact.getEmail());
		}
		if (null != pIndividualContact.getMobilePhoneNumber()) {
			vIndividualContact.setMobilePhoneNumber((pIndividualContact.getMobilePhoneNumber()));
		}

		if (null != pIndividualContact.getTriples()) {
			vIndividualContact.setTriples(pIndividualContact.getTriples());
		}

		if (null != pIndividualContact.getBirthDate()) {
			LOGGER.debug("Entree dans le setDateBirthday");
			vIndividualContact.setBirthDate(pIndividualContact.getBirthDate());
		}

		// A DECOMMENTER POUR TEST
		if (pDeviceId != null) {
			LOGGER.debug("Device detecte..");

			final String vDeviceURI = Constantes.DEVICE_URI_PREFIX + pDeviceId;
			final TripleVO vCurrentTriple = new TripleVO(vDocumentURI, Constantes.HAS_DEVICE, vDeviceURI);

			if (!vIndividualContact.getTriples().getTripleList().contains(vCurrentTriple)) {

				List<TripleVO> tripleListOld = new ArrayList<TripleVO>();
				tripleListOld = vIndividualContact.getTriples().getTripleList();
				LOGGER.debug("Ajout du nouveau device ayant pour uri " + vDeviceURI);

				tripleListOld.add(vCurrentTriple);
				vIndividualContact.setTriples(new TriplesVO(tripleListOld));
			}
		}

		final IIndividualContactDao individualContactDao = new IndividualContactDaoImpl();
		final String postResultUri = individualContactDao.addIndividualContact(vIndividualContact);

		LOGGER.debug("URI du document renvoyé = " + postResultUri);

		final String postResult = pIndividualContactId;

		final INotificationKafkaDao vNotificationKafkaDao = new NotificationKafkaDaoImpl();
		vNotificationKafkaDao.sendMessage(Utils.messageToSend(pIndividualContact));

		return postResult;
	}

}