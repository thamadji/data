package com.bnp.services;

import java.util.Date;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;


/**
* IIndividualContactService - Interface Individual Contact Service.
*
* @version 	1.0
* @see 
* @author 	
* @copyright (C) 2016
* @date 	11/01/2016
* @notes 
*/
public interface IIndividualContactService {

	public String addIndividualContact(IndividualContactVO pIndividualContact, String deviceId, Date pDate) throws TechnicalException;
	public String updateIndividualContact(String pIndividualContactId, String deviceId, IndividualContactVO pIndividualContact, Date pDate) throws TechnicalException;
	public IndividualContactVO getIndividualContactById(String id) throws TechnicalException;
}
