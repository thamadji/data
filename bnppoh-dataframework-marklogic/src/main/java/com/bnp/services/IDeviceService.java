package com.bnp.services;

import java.text.ParseException;
import java.util.Date;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.DeviceVO;
/**
 * 
 * @author thamadji
 *
 */
public interface IDeviceService {
	public String addDevice(DeviceVO pDevice, Date pTimeStamp) throws TechnicalException, ParseException;
}
