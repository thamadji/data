package com.bnp.services;

import com.bnp.dao.ICouponsDao;
import com.bnp.dao.impl.CouponsDaoImpl;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.CouponVO;

/**
 * 
 * @author bdansoko
 *
 */
public class CouponsServiceImpl implements ICouponsService{

	/**
	 * @param pCouponsVO
	 */
	public String addCoupon(CouponVO pCouponsVO) throws TechnicalException {
		CouponsDaoImpl vCouponsDao = new CouponsDaoImpl();
		return vCouponsDao.addCoupon(pCouponsVO);
	}

	/**
	 * @param pCouponsVOID
	 */
	public CouponVO getCoupon(String pCouponsVOID) throws TechnicalException{
		ICouponsDao vCouponDao = new CouponsDaoImpl();
		return vCouponDao.getCoupon(pCouponsVOID);
	}
}
