package com.bnp.services;

import com.bnp.dao.IPointOfSalesDao;
import com.bnp.dao.impl.PointOfSalesDaoImpl;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.PointOfSalesVO;

/**
 * A completer
 * @author jnairain
 *
 */
public class PointOfSalesServiceImpl implements IPointOfSalesService {

	private IPointOfSalesDao mPointOfSalesDao = new PointOfSalesDaoImpl();
	
	/**
	 * A completer
	 * @throws TechnicalException 
	 */
	public String addPointOfSales(PointOfSalesVO pPointOfSalesVO) throws TechnicalException {
		return mPointOfSalesDao.addPointOfSales(pPointOfSalesVO);
	}

	/**
	 * A completer
	 */
	public PointOfSalesVO getPointOfSales(String pPointOfSalesVOID) throws TechnicalException {
		return mPointOfSalesDao.getPointOfSales(pPointOfSalesVOID);
	}

}
