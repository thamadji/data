package com.bnp.services;

import com.bnp.dao.IUserCouponDao;
import com.bnp.dao.impl.UserCouponDaoImpl;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.model.coupon.UserCouponVO;
import com.bnp.tools.Constantes;
import com.bnp.tools.Utils;

/**
 *
 * @author bdansoko
 *
 */
public class UserCouponServiceImpl implements IUserCouponService {

	private final IUserCouponDao mUserCouponDao = new UserCouponDaoImpl();
	private final IIndividualContactService mIndividualContactService = new IndividualContactServiceImpl();

	/**
	 * A completer
	 * 
	 * @param pUserCouponVO
	 * @return
	 * @throws TechnicalException
	 */
	@Override
	public String addUserCoupon(final UserCouponVO pUserCouponVO) throws TechnicalException {

		// Set User coupon properties
		pUserCouponVO.setUserCouponId(Utils.createUniqueId());
		pUserCouponVO.setDocumentURI(Constantes.USER_COUPON_URI_PREFIX + pUserCouponVO.getUserCouponId());
		pUserCouponVO.setTriples(new TriplesVO());
		pUserCouponVO.getTriples().getTripleList()
				.add(new TripleVO(pUserCouponVO.getDocumentURI(), Constantes.PREDICAT_COUPON_STATE,
						Constantes.USER_COUPON_STATE_URI_PREFIX + Constantes.USER_COUPON_STATE_OFFERED));

		// User Coupon DAO (Write)
		String vUserCouponURI = mUserCouponDao.addUserCoupon(pUserCouponVO);

		// Individual contact Dao (Read)
		final IndividualContactVO vIndividualContactVO = mIndividualContactService
				.getIndividualContactById(pUserCouponVO.getIndividualContactId());
		vIndividualContactVO.setDocumentURI(Constantes.INDIVIDUAL_CONTACT_URI_PREFIX + vIndividualContactVO.getId());

		// Add new triple
		vIndividualContactVO.getTriples().getTripleList().add(new TripleVO(vIndividualContactVO.getDocumentURI(),
				Constantes.PREDICAT_COUPON_OWN_COUPON, pUserCouponVO.getDocumentURI()));

		// Individual contact Dao (Write)
		mIndividualContactService.updateIndividualContact(vIndividualContactVO.getId(), null, vIndividualContactVO,
				Utils.getTodayDate());

		// Return User coupon Id
		return pUserCouponVO.getUserCouponId();
	}

	/**
	 * A completer
	 * 
	 * @param pUserCouponID
	 * @return
	 * @throws TechnicalException
	 */
	@Override
	public UserCouponVO getUserCoupon(final String pUserCouponID) throws TechnicalException {
		// User Coupon DAO (Read)
		return mUserCouponDao.getUserCoupon(pUserCouponID);
	}

	/**
	 *
	 * @param pUserCouponID
	 * @throws TechnicalException
	 */
	@Override
	public void archiverUserCoupon(final String pUserCouponID) throws TechnicalException {
		// User Coupon DAO (Read)
		final UserCouponVO vUserCouponVO = mUserCouponDao.getUserCoupon(pUserCouponID);

		// Valid date
		vUserCouponVO.setValidStart(Utils.getTodayDate());

		// Triplet
		if (null != vUserCouponVO.getTriples()) {
			vUserCouponVO.getTriples().removeAllTriples();
		} else {
			vUserCouponVO.setTriples(new TriplesVO());
		}

		vUserCouponVO.getTriples().getTripleList()
				.add(new TripleVO(vUserCouponVO.getDocumentURI(), Constantes.PREDICAT_COUPON_STATE,
						Constantes.USER_COUPON_STATE_URI_PREFIX + Constantes.USER_COUPON_STATE_ARCHIVED));

		// User Coupon DAO (Write)
		mUserCouponDao.addUserCoupon(vUserCouponVO);
	}

	/**
	 *
	 * @param pUserCouponID
	 * @throws TechnicalException
	 */
	@Override
	public void clipperUserCoupon(final String pUserCouponID) throws TechnicalException {
		// User Coupon DAO (Read)
		final UserCouponVO vUserCouponVO = mUserCouponDao.getUserCoupon(pUserCouponID);

		// Valid date
		vUserCouponVO.setValidStart(Utils.getTodayDate());

		// Triplet
		if (null != vUserCouponVO.getTriples()) {
			vUserCouponVO.getTriples().removeAllTriples();
		} else {
			vUserCouponVO.setTriples(new TriplesVO());
		}

		vUserCouponVO.getTriples().getTripleList()
				.add(new TripleVO(vUserCouponVO.getDocumentURI(), Constantes.PREDICAT_COUPON_STATE,
						Constantes.USER_COUPON_STATE_URI_PREFIX + Constantes.USER_COUPON_STATE_CLIPPED));

		// User Coupon DAO (Write)
		mUserCouponDao.addUserCoupon(vUserCouponVO);
	}
}
