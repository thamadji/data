package com.bnp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.Transaction;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;

public class CreateDoc {

	public static void main(final String[] args) throws JsonProcessingException, IOException {
		DatabaseClient dbClient = null;
		Transaction tx = null;
		try {
			dbClient = DatabaseClientFactory.newClient("localhost", 8040, "admin", "admin", Authentication.DIGEST);

			// BEGIN TRANSACTION
			tx = dbClient.openTransaction();

			final String URI = "/BNP/thisIsATest.json";

			final ObjectMapper mapper = new ObjectMapper();
			final JsonNode doc = mapper.readTree(
					"{\"id\":\"100\",\"firstName\":\"Gael\",\"lastName\":\"Monfils\",\"addressIC\":{\"streetName\":\"Horsen Ferry\",\"cityName\":\"Bâle\",\"cityZipcode\":\"01118\"}}");
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().addAll("BNP");
			// ADD properties
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			metadata.getProperties().put("date-time", sdf.format(new Date()));
			final JSONDocumentManager docMgr = dbClient.newJSONDocumentManager();
			docMgr.write(URI, metadata, new JacksonHandle(doc), tx);

			// CREATE CR (TRIPLE)
			final XMLDocumentManager docMgrXML = dbClient.newXMLDocumentManager();
			final String triples = "<cr><localisation lat=\"1\" lon=\"2\"/>"
					+ "<sem:triples xmlns:sem=\"http://marklogic.com/semantics\"> " + "<sem:triple>" + "<sem:subject>"
					+ URI + "</sem:subject>" + "<sem:predicate>http://bnp-ontology/cr/nom</sem:predicate>"
					+ "<sem:object datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + doc.get("lastName")
					+ "</sem:object>" + "</sem:triple> " + "<sem:triple>" + "<sem:subject>" + URI + "</sem:subject>"
					+ "<sem:predicate>http://bnp-ontology/cr/prenom</sem:predicate>"
					+ "<sem:object datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + doc.get("firstName")
					+ "</sem:object>" + "</sem:triple> " + "</sem:triples>" + "</cr>";
			metadata = new DocumentMetadataHandle();
			metadata.getCollections().addAll("BNP-CR");
			docMgrXML.write("/BNP/triples.xml", metadata, new StringHandle(triples), tx);

			// if (true) throw new RuntimeException();

			// COMMIT TRANSACTION
			tx.commit();
		} catch (final Exception e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			if (dbClient != null) {
				dbClient.release();
			}
		}
	}

}
