package com.bnp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public abstract class MarkLogicVO {

	// @XStreamOmitField
	@XStreamAlias("documentURI")
	@JsonIgnore
	private String documentURI;

	/**
	 * A completer
	 */
	@XStreamAlias("sem:triples")
	@JsonIgnore
	private TriplesVO triples;

	@JsonIgnore
	public String getDocumentURI() {
		return documentURI;
	}

	@JsonIgnore
	public void setDocumentURI(final String _documentURI) {
		documentURI = _documentURI;
	}

	/**
	 * A completer
	 * 
	 * @return
	 */
	public TriplesVO getTriples() {
		return triples;
	}

	/**
	 * A completer
	 * 
	 * @param triples
	 */
	public void setTriples(final TriplesVO triples) {
		this.triples = triples;
	}
}
