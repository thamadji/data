package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * A completer
 *
 * @author jnairain
 *
 */
public class TripleVO {

	/**
	 * A completer
	 */
	@XStreamAlias("sem:subject")
	private String subject;

	/**
	 * A completer
	 */
	@XStreamAlias("sem:predicate")
	private String predicate;

	/**
	 * A completer
	 */
	@XStreamAlias("sem:object")
	private String object;

	/**
	 * A completer
	 *
	 * @param pSubject
	 * @param pPredicate
	 * @param pObject
	 */
	public TripleVO(final String pSubject, final String pPredicate, final String pObject) {
		subject = pSubject;
		predicate = pPredicate;
		object = pObject;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * A completer
	 *
	 * @param subject
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/**
	 * A completer
	 *
	 * @return
	 */

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}

	/**
	 * A completer
	 *
	 * @param predicate
	 */
	public void setPredicate(final String predicate) {
		this.predicate = predicate;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getObject() {
		return object;
	}

	/**
	 * A completer
	 *
	 * @param object
	 */
	public void setObject(final String object) {
		this.object = object;
	}
}
