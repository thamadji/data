package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("individualContact")
public class IndividualContactVO extends BiTemporalVO {

	@XStreamAlias("id")
	private String id;

	@XStreamAlias("firstName")
	private String firstName;

	@XStreamAlias("lastName")
	private String lastName;

	@XStreamAlias("email")
	private String email;

	@XStreamAlias("mobilePhoneNumber")
	private String mobilePhoneNumber;

	@XStreamAlias("birthDate")
	private String birthDate;

	// @XStreamOmitField
	// private String deviceId;

	/**
	 * IndividualContact - Constructeur vide
	 *
	 * @return
	 * @param
	 * @exception @see
	 * @date 10/01/2016
	 * @note
	 */
	public IndividualContactVO() {
	}

	/**
	 * IndividualContact - Constructeur non vide
	 *
	 * @return
	 * @param
	 * @exception @see
	 * @date 10/01/2016
	 * @note
	 */
	public IndividualContactVO(final String id, final String firstName, final String lastName,
			final AddressVO adress_contact) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * @return String id
	 * @param
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return
	 * @param id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * @return String firstName
	 * @param
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 * @param firstName
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return String lastName
	 * @param
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 * @param lastName
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	// public String getDeviceId() {
	// return deviceId;
	// }
	//
	// public void setDeviceId(String deviceId) {
	// this.deviceId = deviceId;
	// }

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(final String birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(final String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

}
