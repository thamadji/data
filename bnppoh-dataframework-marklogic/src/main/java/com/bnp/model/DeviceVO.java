package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("device")
public class DeviceVO extends BiTemporalVO {
	
	private String deviceId ;
	
	@XStreamAlias("geoposition")
	private LocalizationVO localization;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public LocalizationVO getLocalization() {
		return localization;
	}

	public void setLocalization(LocalizationVO localization) {
		this.localization = localization;
	}
}
