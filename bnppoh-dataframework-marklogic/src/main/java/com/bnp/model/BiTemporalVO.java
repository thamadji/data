package com.bnp.model;

import java.util.Date;

import com.bnp.tools.DateConverter;
import com.bnp.tools.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 *
 * @author thamadji
 *
 */
@XStreamAlias("temporal")
public class BiTemporalVO extends MarkLogicVO {

	@XStreamAlias("systemStart")
	@JsonIgnore
	// @XStreamConverter(DateConverter.class)
	private String systemStart = "";

	// @XStreamConverter(DateConverter.class)
	@XStreamAlias("systemEnd")
	@JsonIgnore
	private String systemEnd = "";

	@XStreamConverter(DateConverter.class)
	@XStreamAlias("validStart")
	@JsonIgnore
	private Date validStart = Utils.getTodayDate();

	@XStreamConverter(DateConverter.class)
	@XStreamAlias("validEnd")
	@JsonIgnore
	private Date validEnd = Utils.infinitedate;

	public String getSystemStart() {
		return systemStart;
	}

	@JsonIgnore
	public void setSystemStart(final String systemStart) {
		this.systemStart = systemStart;
	}

	@JsonIgnore
	public String getSystemEnd() {
		return systemEnd;
	}

	@JsonIgnore
	public void setSystemEnd(final String systemEnd) {
		this.systemEnd = systemEnd;
	}

	@JsonIgnore
	public Date getValidStart() {
		return validStart;
	}

	@JsonIgnore
	public void setValidStart(final Date validStart) {
		this.validStart = validStart;
	}

	@JsonIgnore
	public Date getValidEnd() {
		return validEnd;
	}

	@JsonIgnore
	public void setValidEnd(final Date validEnd) {
		this.validEnd = validEnd;
	}
}
