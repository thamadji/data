package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("address")
public class AddressVO {
	@XStreamAlias("streetName")
	private String streetName;
	@XStreamAlias("cityName")
	private String cityName;
	@XStreamAlias("cityZipcode")
	private String cityZipcode;
	
	
	/**
	* Address - Constructeur vide
	*             		
	* @return      
	* @param  
	* @exception    
	* @see 
	* @date 		10/01/2016
	* @note     
	*/
	public AddressVO() {}
	
	
	/**
	* Address - Constructeur non vide
	*             		
	* @return      
	* @param  
	* @exception    
	* @see 
	* @date 		10/01/2016
	* @note     
	*/
	public AddressVO(String streetName, String cityName, String cityZipcode) {
		super();
		this.streetName = streetName;
		this.cityName = cityName;
		this.cityZipcode = cityZipcode;
	}
	
	
	/**
	* @return String streetName       
	* @param       
	*/
	public String getStreetName() {
		return streetName;
	}
	
	
	/**
	* @return        
	* @param      streetName 
	*/
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	
	
	/**
	* @return String cityName       
	* @param       
	*/
	public String getCityName() {
		return cityName;
	}
	
	
	/**
	* @return        
	* @param      cityName 
	*/
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
	/**
	* @return String cityZipcode       
	* @param       
	*/
	public String getCityZipcode() {
		return cityZipcode;
	}
	
	
	/**
	* @return        
	* @param      cityZipcode 
	*/
	public void setCityZipcode(String cityZipcode) {
		this.cityZipcode = cityZipcode;
	}
	
}

