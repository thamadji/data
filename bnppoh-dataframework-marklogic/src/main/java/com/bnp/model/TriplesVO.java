package com.bnp.model;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * A completer
 * @author jnairain
 *
 */
public class TriplesVO {

	/**
	 * A completer
	 */
	@XStreamImplicit(itemFieldName="triple")
	private List<TripleVO> tripleList = new ArrayList<TripleVO>();

	/**
	 * A completer
	 */
	public TriplesVO () {
	}
	
	/**
	 * A completer
	 * @param pTriplesList
	 */
	public TriplesVO (List<TripleVO> pTriplesList) {
		this.setTripleList(pTriplesList);
	}
	
	/**
	 * A completer
	 * @return
	 */
	public List<TripleVO> getTripleList() {
		return tripleList;
	}

	/**
	 * A completer
	 * @param tripleList
	 */
	public void setTripleList(List<TripleVO> tripleList) {
		this.tripleList = tripleList;
	}
	
	public void removeAllTriples() {
		this.tripleList = new ArrayList<TripleVO>();
	}
}
