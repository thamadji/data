package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * A completer
 *
 * @author jnairain
 *
 */
@XStreamAlias("pointOfSales")
public class PointOfSalesVO extends BiTemporalVO {

	private static final String REVERSE_QUERY = "{cts:element-pair-geospatial-query(xs:QName(\"geoposition\"), xs:QName(\"latitude\"), xs:QName(\"longitude\"), cts:circle($radius * 0.62137119, cts:point($my_latitude, $my_longitude)))}";
	/**
	 * A completer
	 */
	@XStreamAlias("id")
	private String pointOfSalesID;

	// /**
	// * A completer
	// */
	// @XStreamAlias("geo_point_2D")
	// private GeoPoint2DVO geoPoint2D;

	/**
	 * A completer
	 */
	@XStreamAlias("geoposition")
	private LocalizationVO localizationVO;

	/**
	 * A completer
	 */
	@XStreamAlias("nom")
	private String name;

	/**
	 * A completer
	 */
	@XStreamAlias("departement")
	private int zipCode;

	/**
	 * A completer
	 */
	@XStreamAlias("openDays")
	private OpenDaysVO openDays;

	/**
	 * A completer
	 */
	@XStreamAlias("devicesCibles")
	private String reverseQuery;

	/**
	 * A completer
	 *
	 * @param pPointOfSalesID
	 * @param pGeoPoint2D
	 * @param pName
	 * @param pZipCode
	 * @param pOpenDays
	 * @param pTriples
	 */
	public PointOfSalesVO(final String pPointOfSalesID,
			// GeoPoint2DVO pGeoPoint2D,
			final LocalizationVO pLocalizationVO, final String pDistance, final String pName, final int pZipCode,
			final OpenDaysVO pOpenDays, final TriplesVO pTriples) {
		super.setTriples(pTriples);
		pointOfSalesID = pPointOfSalesID;
		// this.setGeoPoint2D(pGeoPoint2D);
		localizationVO = pLocalizationVO;
		name = pName;
		zipCode = pZipCode;
		openDays = pOpenDays;
		reverseQuery = REVERSE_QUERY.replaceAll("\\$radius", pDistance)
				.replaceAll("\\$my_latitude", pLocalizationVO.getLatitude())
				.replaceAll("\\$my_longitude", pLocalizationVO.getLongitude());
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getPointOfSalesID() {
		return pointOfSalesID;
	}

	/**
	 * A completer
	 *
	 * @param pointOfSalesID
	 */
	public void setPointOfSalesID(final String pointOfSalesID) {
		this.pointOfSalesID = pointOfSalesID;
	}

	// /**
	// * A completer
	// * @return
	// */
	// public GeoPoint2DVO getGeoPoint2D() {
	// return geoPoint2D;
	// }
	//
	// /**
	// * A completer
	// * @param geoPoint2D
	// */
	// public void setGeoPoint2D(GeoPoint2DVO geoPoint2D) {
	// this.geoPoint2D = geoPoint2D;
	// }

	/**
	 * A completer
	 *
	 * @return
	 */
	public LocalizationVO getLocalizationVO() {
		return localizationVO;
	}

	/**
	 * A completer
	 *
	 * @param localizationVO
	 */
	public void setLocalizationVO(final LocalizationVO localizationVO) {
		this.localizationVO = localizationVO;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * A completer
	 *
	 * @param name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public int getZipCode() {
		return zipCode;
	}

	/**
	 * A completer
	 *
	 * @param zipCode
	 */
	public void setZipCode(final int zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public OpenDaysVO getOpenDays() {
		return openDays;
	}

	/**
	 * A completer
	 *
	 * @param openDays
	 */
	public void setOpenDays(final OpenDaysVO openDays) {
		this.openDays = openDays;
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public String getReverseQuery() {
		return reverseQuery;
	}

	/**
	 * A completer
	 *
	 * @param reverseQuery
	 */
	public void setReverseQuery(final String reverseQuery) {
		this.reverseQuery = reverseQuery;
	}

}
