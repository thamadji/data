package com.bnp.model;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * A completer
 * @author jnairain
 *
 */
public class OpenDaysVO {

	/**
	 * A completer
	 */
	@XStreamImplicit(itemFieldName="openDay")
	private List<String> openDaysList = new ArrayList<String>();
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private Boolean rdf = Boolean.TRUE;
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String predicate = "hasOpenDay";
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String prefix = "/openDays";
	
	/**
	 * A completer
	 * @param pOpenDaysList
	 */
	public OpenDaysVO (
			List<String> pOpenDaysList){
		this.setOpenDaysList(pOpenDaysList);
	}
	
	/**
	 * A completer
	 * @param pOpenDaysList
	 * @param pRdf
	 * @param pPredicate
	 * @param pPrefix
	 */
	public OpenDaysVO (
			List<String> pOpenDaysList,
			Boolean pRdf,
			String pPredicate,
			String pPrefix){
		this.setOpenDaysList(pOpenDaysList);
		this.setRdf(pRdf);
		this.setPredicate(pPredicate);
		this.setPrefix(pPrefix);
	}
	
	/**
	 * A completer
	 * @return
	 */
	public List<String> getOpenDaysList() {
		return openDaysList;
	}
	
	/**
	 * A completer
	 * @param openDaysList
	 */
	public void setOpenDaysList(List<String> openDaysList) {
		this.openDaysList = openDaysList;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public Boolean getRdf() {
		return rdf;
	}
	
	/**
	 * A completer
	 * @param rdf
	 */
	public void setRdf(Boolean rdf) {
		this.rdf = rdf;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}
	
	/**
	 * A completer
	 * @param predicate
	 */
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public String getPrefix() {
		return prefix;
	}
	
	/**
	 * A completer
	 * @param prefix
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
