package com.bnp.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.bnp.tools.Configuration;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.admin.ExtensionMetadata;
import com.marklogic.client.admin.TransformExtensionsManager;
import com.marklogic.client.io.InputStreamHandle;

public class InstallingTransform {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DatabaseClient client = DatabaseClientFactory.newClient(Configuration.DB_HOST, Configuration.DB_PORT,
				Configuration.DB_USER_NAME, Configuration.DB_PASSWORD, Authentication.DIGEST);
		
		TransformExtensionsManager transMgr = 
			    client.newServerConfigManager().newTransformExtensionsManager();
		//remove transform
//		transMgr.deleteTransform("transform_v1");
		
		ExtensionMetadata metadata = new ExtensionMetadata();
		metadata.setTitle("Transformation");
		metadata.setDescription("Mapping ontologie");
		metadata.setProvider("Boubacar");
		metadata.setVersion("0.1");
		
		FileInputStream transStream = null;
		try {
			transStream = new FileInputStream("D:\\transform_to_semantic_v1.xqy");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		InputStreamHandle handle = new InputStreamHandle(transStream);
		
		transMgr.writeXQueryTransform("transform_to_semantic_v1", handle, metadata);
		
		client.release();
	}

}
