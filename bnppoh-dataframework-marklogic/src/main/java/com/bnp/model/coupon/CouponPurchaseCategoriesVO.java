package com.bnp.model.coupon;

import java.util.ArrayList;
import java.util.List;

import com.bnp.tools.Constantes;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CouponPurchaseCategoriesVO {

	@XStreamImplicit(itemFieldName="purchaseCategorie")
	private List<String> purchaseCategoriesList = new ArrayList<String>();
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private Boolean rdf = Boolean.TRUE;
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String predicate = "http://www.semanticweb.org/BNP/ontologies/2015/12/ERMW#hasPurchaseCategory";
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String prefixe = "http://www.semanticweb.org/BNP/ontologies/2015/12/ERMW#";
	
	/**
	 * 
	 */
	@XStreamAsAttribute
	private String keep = "true";
	
	/**
	 * 
	 * @param pPurchaseCategoriesList
	 */
	public CouponPurchaseCategoriesVO (
			List<String> pPurchaseCategoriesList){
		this.setPurchaseCategoriesList(pPurchaseCategoriesList);
	}
	
	/**
	 * 
	 * @param pPurchaseCategoriesList
	 * @param pRdf
	 * @param pPredicate
	 * @param pPrefix
	 */
	public CouponPurchaseCategoriesVO (
			List<String> pPurchaseCategoriesList,
			Boolean pRdf,
			String pPredicate,
			String pPrefix){
		this.setPurchaseCategoriesList(pPurchaseCategoriesList);
		this.setRdf(pRdf);
		this.setPredicate(pPredicate);
		this.setPrefix(pPrefix);
	}

	/**
	 * A completer
	 * @return
	 */
	public List<String> getPurchaseCategoriesList() {
		return purchaseCategoriesList;
	}
	
	/**
	 * 
	 * @param purchaseCategoriesList
	 */
	public void setPurchaseCategoriesList(List<String> purchaseCategoriesList) {
		for (String s : purchaseCategoriesList){
			this.purchaseCategoriesList.add(Constantes.PURCHASES_CATEGORIES_URI_PREFIX + s) ;
		}
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getRdf() {
		return rdf;
	}

	/**
	 * 
	 * @param rdf
	 */
	public void setRdf(Boolean rdf) {
		this.rdf = rdf;
	}

	/**
	 * 
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}

	/**
	 * 
	 * @param predicate
	 */
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	/**
	 * 
	 * @return
	 */
	public String getPrefix() {
		return prefixe;
	}

	/**
	 * 
	 * @param prefix
	 */
	public void setPrefix(String prefix) {
		this.prefixe = prefix;
	}

	@Override
	public String toString() {
		return "CouponPurchaseCategoriesVO [purchaseCategoriesList=" + purchaseCategoriesList + ", rdf=" + rdf
				+ ", predicate=" + predicate + ", prefix=" + prefixe + "]";
	}
	
}
