package com.bnp.model.coupon;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 *
 * @author bdansoko
 *
 */

public class Source {

	@XStreamAlias("Value")
	private String sourceValue;

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private final Boolean rdf = Boolean.TRUE;

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private final String predicate = "isFromFile";

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private final String prefixe = "/coupon/files/";

	/**
	 *
	 */
	@XStreamAsAttribute
	private final String keep = "true";

	/**
	 *
	 * @param pSourceValue
	 */
	public Source(final String pSourceValue) {
		this.setSourceValue(pSourceValue);
	}

	/**
	 *
	 * @return
	 */
	public String getSourceValue() {
		return sourceValue;
	}

	/**
	 *
	 * @param sourceValue
	 */
	public void setSourceValue(final String sourceValue) {
		this.sourceValue = sourceValue;
	}

}
