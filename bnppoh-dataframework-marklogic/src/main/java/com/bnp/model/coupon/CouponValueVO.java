package com.bnp.model.coupon;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


/**
 * A completer
 * @author bdansoko
 *
 */
@XStreamAlias("valueCoupon")
public class CouponValueVO {

	/**
	 * A completer
	 */
	@XStreamAlias("value")
	private String valueCoupon;
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private Boolean rdf = Boolean.TRUE;
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String predicate = "http://www.semanticweb.org/BNP/ontologies/2015/12/ERMW#hasvalueCoupon";
	
	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String dataType = "http://www.w3.org/2001/XMLSchema#String";
	
	/**
	 * 
	 */
	@XStreamAsAttribute
	private String keep = "true";
	
	
	/**
	 * 
	 * @param pvalue
	 * @param pRdf
	 * @param pPredicate
	 * @param pDataType
	 */
	public CouponValueVO (
			String pvalue,
			Boolean pRdf,
			String pPredicate,
			String pDataType) {
		this.setvalueCoupon(pvalue);
		this.setRdf(pRdf);
		this.setPredicate(pPredicate);
		this.setDataType(pDataType);
	}
	
	/**
	 * 
	 * @param pValue
	 */
	public CouponValueVO (String pValue) {
		this.setvalueCoupon(pValue);
	}

	/**
	 * 
	 * @return
	 */
	public String getvalueCoupon() {
		return valueCoupon;
	}

	/**
	 * 
	 * @param valueCoupon
	 */
	public void setvalueCoupon(String valueCoupon) {
		this.valueCoupon = valueCoupon;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getRdf() {
		return rdf;
	}

	/**
	 * 
	 * @param rdf
	 */
	public void setRdf(Boolean rdf) {
		this.rdf = rdf;
	}

	/**
	 * 
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}

	/**
	 * 
	 * @param predicate
	 */
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * 
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	@Override
	public String toString() {
		return "ValueCouponVO [valueCoupon=" + valueCoupon + ", rdf=" + rdf + ", predicate=" + predicate + ", dataType="
				+ dataType + "]";
	}
	
	
	
}
