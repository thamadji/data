package com.bnp.model.coupon;

import com.bnp.model.BiTemporalVO;
import com.bnp.model.TriplesVO;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * A completer
 * @author bdansoko
 *
 */


@XStreamAlias("coupon")
public class CouponVO extends BiTemporalVO {

	/**
	 * A completer
	 */
	@XStreamAlias("couponId")
	private String couponId;
	
	/**
	 * A completer
	 */
	@XStreamAlias("image")
	private String image;
	
	/**
	 * A completer
	 */
	@XStreamAlias("description")
	private String description;
	
	/**
	 * A completer
	 */
	@XStreamAlias("value")
	private CouponValueVO value;
	
	/**
	 * A completer
	 */
	@XStreamAlias("purchase_categories")
	private CouponPurchaseCategoriesVO purchaseCategories;
	
	/**
	 * A completer
	 */
	@XStreamAlias("pointOfSales")
	private CouponPointOfsalesVO pointOfSales;
	
	
	/**
	 * 
	 * A completer
	 */
	public CouponVO() {
	
	}
	/**
	 * 
	 * @param couponId
	 * @param image
	 * @param description
	 * @param value
	 * @param purchaseCategories
	 * @param pointOfSales
	 * @param pTriples
	 */
	public CouponVO(String couponId, String image, String description, CouponValueVO value,
			CouponPurchaseCategoriesVO purchaseCategories, CouponPointOfsalesVO pointOfSales, TriplesVO pTriples) {
		super();
		this.couponId = couponId;
		this.image = image;
		this.description = description;
		this.value = value;
		this.purchaseCategories = purchaseCategories;
		this.pointOfSales = pointOfSales;
		this.setTriples(pTriples);
	}

	/**
	 * 
	 * @return
	 */
	public String getCouponId() {
		return couponId;
	}

	/**
	 * 
	 * @param couponId
	 */
	public void setId(String couponId) {
		this.couponId = couponId;
	}

	/**
	 * 
	 * @return
	 */
	public String getImage() {
		return image;
	}

	/**
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public CouponValueVO getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 */
	public void setCouponValue(CouponValueVO value) {
		this.value = value;
	}

	/**
	 * 
	 * @return
	 */
	public CouponPurchaseCategoriesVO getPurchaseCategories() {
		return purchaseCategories;
	}

	/**
	 * 
	 * @param purchaseCategories
	 */
	public void setPurchaseCategories(CouponPurchaseCategoriesVO purchaseCategories) {
		this.purchaseCategories = purchaseCategories;
	}

	/**
	 * 
	 * @return
	 */
	public CouponPointOfsalesVO getPointOfSales() {
		return pointOfSales;
	}

	/**
	 * 
	 * @param pointOfSales
	 */
	public void setCouponPointOfSales(CouponPointOfsalesVO pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	@Override
	public String toString() {
		return "CouponsVO [couponId=" + couponId + ", couponImage=" + image + ", couponDescription="
				+ description + ", couponValue=" + value + ", couponPurchaseCategories="
				+ purchaseCategories + ", couponPointOfSales=" + pointOfSales + "]";
	}

	
	
}
