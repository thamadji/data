package com.bnp.model.coupon;

import java.util.ArrayList;
import java.util.List;

import com.bnp.tools.Constantes;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CouponPointOfsalesVO {

	@XStreamImplicit(itemFieldName = "pointOfsale")
	private List<String> pointOfsalesList = new ArrayList<String>();

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private Boolean rdf = Boolean.TRUE;

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String predicate = "http://www.semanticweb.org/BNP/ontologies/2015/12/ERMW#isApplicable";

	/**
	 * A completer
	 */
	@XStreamAsAttribute
	private String prefixe = "http://www.semanticweb.org/BNP/ontologies/2015/12/ERMW#";

	/**
	 *
	 */
	@XStreamAsAttribute
	private final String keep = "true";

	/**
	 *
	 * @param pPointOfsalesList
	 */
	public CouponPointOfsalesVO(final List<String> pPointOfsalesList) {
		pointOfsalesList = pPointOfsalesList;
	}

	/**
	 *
	 * @param pPointOfsalesList
	 * @param pRdf
	 * @param pPredicate
	 * @param pPrefix
	 */
	public CouponPointOfsalesVO(final List<String> pPointOfsalesList, final Boolean pRdf, final String pPredicate,
			final String pPrefix) {
		this.setPointOfsalesList(pPointOfsalesList);
		this.setRdf(pRdf);
		this.setPredicate(pPredicate);
		this.setPrefix(pPrefix);
	}

	/**
	 *
	 * @return
	 */
	public List<String> getPointOfsalesList() {
		return pointOfsalesList;
	}

	/**
	 *
	 * @param pointOfsalesList
	 */
	public void setPointOfsalesList(final List<String> pointOfsalesList) {
		for (final String s : pointOfsalesList) {
			this.pointOfsalesList.add(Constantes.POINT_OF_SALES_URI_PREFIX + s);
		}
	}

	/**
	 *
	 * @return
	 */
	public Boolean getRdf() {
		return rdf;
	}

	/**
	 *
	 * @param rdf
	 */
	public void setRdf(final Boolean rdf) {
		this.rdf = rdf;
	}

	/**
	 *
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}

	/**
	 *
	 * @param predicate
	 */
	public void setPredicate(final String predicate) {
		this.predicate = predicate;
	}

	/**
	 *
	 * @return
	 */
	public String getPrefix() {
		return prefixe;
	}

	/**
	 *
	 * @param prefix
	 */
	public void setPrefix(final String prefix) {
		this.prefixe = prefix;
	}

	@Override
	public String toString() {
		return "CouponPointOfsalesVO [pointOfsalesList=" + pointOfsalesList + ", rdf=" + rdf + ", predicate="
				+ predicate + ", prefix=" + prefixe + "]";
	}

}
