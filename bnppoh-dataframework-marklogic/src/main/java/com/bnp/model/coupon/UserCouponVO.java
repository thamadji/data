package com.bnp.model.coupon;

import com.bnp.model.BiTemporalVO;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("userCoupon")
public class UserCouponVO extends BiTemporalVO {

	/**
	 * A compléter
	 */
	@XStreamAlias("userCouponId")
	private String userCouponId;

	/**
	 * A compléter
	 */
	@XStreamAlias("couponId")
	private String couponId;
	
	/**
	 * A compléter
	 */
	@XStreamAlias("individualContactId")
	private String individualContactId;
	
	/**
	 * A compléter
	 * @return
	 */
	public String getUserCouponId() {
		return userCouponId;
	}

	/**
	 * A compléter
	 * @param userCouponId
	 */
	public void setUserCouponId(String userCouponId) {
		this.userCouponId = userCouponId;
	}

	/**
	 * A compléter
	 * @return
	 */
	public String getCouponId() {
		return couponId;
	}

	/**
	 * A compléter
	 * @param couponId
	 */
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	/**
	 * A compléter
	 * @return
	 */
	public String getIndividualContactId() {
		return individualContactId;
	}

	/**
	 * A compléter
	 * @param individualContactId
	 */
	public void setIndividualContactId(String individualContactId) {
		this.individualContactId = individualContactId;
	}
}
