package com.bnp.model;

public class WebServiceEntity {
	private String individualContactId;
	private String pointOfSalesId;
	private String deviceId;
	
	public WebServiceEntity() {}
	
	public WebServiceEntity(String individualContactId, String pointOfSaleId, String deviceId) {
		this.individualContactId = individualContactId;
		this.pointOfSalesId = pointOfSaleId;
		this.deviceId = deviceId;
	}
	
	public String getIndividualContactId() {
		return individualContactId;
	}

	public void setIndividualContactId(String individualContactId) {
		this.individualContactId = individualContactId;
	}

	public String getPointOfSalesId() {
		return pointOfSalesId;
	}

	public void setPointOfSalesId(String pointOfSalesId) {
		this.pointOfSalesId = pointOfSalesId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
