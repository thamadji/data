package com.bnp.model;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;

/**
 * A completer
 * @author jnairain
 *
 */
@XStreamConverter(value=ToAttributedValueConverter.class, strings={"geoPoint2D"})
public class GeoPoint2DVO {

	/**
	 * A completer
	 */
	private String geoPoint2D;
	
	/**
	 * A completer
	 */
	private Boolean rdf = Boolean.TRUE;
	
	/**
	 * A completer
	 */
	private String predicate = "iso6709";
	
	/**
	 * A completer
	 */
	private String dataType = "String";
	
	/**
	 * A completer
	 * @param pGeoPoint2D
	 */
	public GeoPoint2DVO (
			String pGeoPoint2D) {
		this.setGeoPoint2D(pGeoPoint2D);
	}
	
	/**
	 * A completer
	 * @param pGeoPoint2D
	 * @param pRdf
	 * @param pPredicate
	 * @param pDataType
	 */
	public GeoPoint2DVO (
			String pGeoPoint2D,
			Boolean pRdf,
			String pPredicate,
			String pDataType) {
		this.setGeoPoint2D(pGeoPoint2D);
		this.setRdf(pRdf);
		this.setPredicate(pPredicate);
		this.setDataType(pDataType);
	}

	/**
	 * A completer
	 * @return
	 */
	public String getGeoPoint2D() {
		return geoPoint2D;
	}
	
	/**
	 * A completer
	 * @param geoPoint2D
	 */
	public void setGeoPoint2D(String geoPoint2D) {
		this.geoPoint2D = geoPoint2D;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public Boolean getRdf() {
		return rdf;
	}
	
	/**
	 * A completer
	 * @param rdf
	 */
	public void setRdf(Boolean rdf) {
		this.rdf = rdf;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public String getPredicate() {
		return predicate;
	}
	
	/**
	 * A completer
	 * @param predicate
	 */
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	
	/**
	 * A completer
	 * @return
	 */
	public String getDataType() {
		return dataType;
	}
	
	/**
	 * A completer
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

}
