package com.bnp.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 
 * @author thamadji
 *
 */
public class LocalizationVO  {
	
	@XStreamAlias("latitude")
	private String latitude;
	
	@XStreamAlias("longitude")
	private String longitude;

	public LocalizationVO() {
	}
	/**
	 * A completer
	 * @param pLatitude
	 * @param pLongitude
	 */
	public LocalizationVO (String pLatitude, String pLongitude) {
		this.setLatitude(pLatitude);
		this.setLongitude(pLongitude);		
	}
	
	/**
	 * A completer
	 * @return
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * A completer
	 * @param pLatitude
	 */
	public void setLatitude(String pLatitude) {
		this.latitude = pLatitude;
	}

	/**
	 * A completer
	 * @return
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * A completer
	 * @param pLongitude
	 */
	public void setLongitude(String pLongitude) {
		this.longitude = pLongitude;
	}
}
