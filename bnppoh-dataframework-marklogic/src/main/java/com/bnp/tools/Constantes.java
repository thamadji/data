package com.bnp.tools;

/**
 * 
 * @author Johan
 *
 */
public class Constantes {

	public static final String POINT_OF_SALES_COLLECTION = "pointOfSales";
	public static final String POINT_OF_SALES_URI_PREFIX = "/pointOfSales/";
	public static final String POINT_OF_SALES_FILE_URI_PREFIX = "/pointOfSales/files/";
	
	public static final String INDIVIDUAL_CONTACT = "individualContact";
	public static final String INDIVIDUAL_CONTACT_COLLECTION = "individualContact";
	public static final String INDIVIDUAL_CONTACT_URI_PREFIX = "/individualContact/";
	public static final String BITEMPORAL_COLLECTION = "bitemporal";

	public static final String HAS_DEVICE = "hasDevice";
	
	public static final String DEVICE_COLLECTION = "device";

	public static final String DEVICE_URI_PREFIX = "/device/";

	/**
	 * coupon
	 */
	public static final String COUPON_COLLECTION = "coupon";
	public static final String COUPON_URI_PREFIX = "/coupon/";
	public static final String COUPON_FILE_URI_PREFIX = "/coupon/files/";

	
	/*
	 * Purchases categories
	 */
	public static final String PURCHASES_CATEGORIES_URI_PREFIX = "/PurchaseCategory/";
	
	/*
	 * user coupon
	 */
	public static final String USER_COUPON_COLLECTION = "userCoupon";
	public static final String USER_COUPON_URI_PREFIX = "/userCoupon/";
	
	public static final String USER_COUPON_STATE_URI_PREFIX = "/userCouponState/";
	public static final String USER_COUPON_STATE_OFFERED = "Offered";
	public static final String USER_COUPON_STATE_ARCHIVED = "Archived";
	public static final String USER_COUPON_STATE_CLIPPED = "Clipped";
	
	/*
	 * Predicats
	 */
	public static final String PREDICAT_IS_FROM_FILE = "isFromFile";
	public static final String PREDICAT_COUPON_OWN_COUPON = "ownCoupon";
	public static final String PREDICAT_COUPON_STATE = "stateCoupon";
	
	/*
	 * WebService de notificaiton 
	 */
	public static final String WS_EVENT_URI = "https://bnppoh-api.fr.capgemini.com/v0_0_1/da_fait_correlation/event";	
	public static final String WS_EVENT_PATH = "/utilisateur_dans_magasin";


}
