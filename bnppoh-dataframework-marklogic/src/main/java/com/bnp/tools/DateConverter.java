package com.bnp.tools;

import java.util.Date;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DateConverter implements Converter {

	private static final Logger LOGGER = Logger.getLogger(DateConverter.class);

	public boolean canConvert(Class clazz) {
		return Date.class.isAssignableFrom(clazz);

	}

	@Override
	public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
		final Date oldDate = (Date) source;
		String newDate = "";
		LOGGER.debug("Date recue : " + source);

		try {
			newDate = Utils.toMarkLogicDateFormat(oldDate);
		} catch (TechnicalException e) {
			e.printStackTrace();
		}
		writer.setValue(newDate);
	}


	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Date vDate = null;

		try {
			vDate = Utils.parseDate(reader.getValue());
		} catch (final TechnicalException e) {
			LOGGER.error("Utils.parseDate", e);
		}

		return vDate;
	}

}
