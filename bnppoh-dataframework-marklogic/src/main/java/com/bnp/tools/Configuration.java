package com.bnp.tools;

/**
 * Configuration - Configuration utilisée pour se connecter à ML.
 *
 * @version 1.0
 * @see
 * @author
 * @copyright (C) 2016
 * @date 11/01/2016
 * @notes
 */
public final class Configuration {

	public static final String DB_HOST;
	public static final int DB_PORT;
	public static final String DB_USER_NAME;
	public static final String DB_PASSWORD;
	public static final Integer DB_MAX_CONNECTIONS;

	static {
		DB_HOST = "frparvm-dn-11.corp.capgemini.com";
		DB_PORT = 8011;
		DB_USER_NAME = "admin";
		DB_PASSWORD = "password";
		DB_MAX_CONNECTIONS = 30;
	}

	private Configuration() {
	}

}
