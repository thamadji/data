package com.bnp.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;

public class Utils {

	/**
	 * A completer
	 */
	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
			Locale.FRANCE);

	/**
	 * A completer
	 */
	public static Date infinitedate;

	static {
		try {
			infinitedate = SIMPLE_DATE_FORMAT.parse("9999-12-31T23:59:59Z");
		} catch (final ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public static Date getTodayDate() {
		return new Date();
	}

	public static String toMarkLogicDateFormat(final Date date) throws TechnicalException {
		return SIMPLE_DATE_FORMAT.format(date);
	}

	public static Date parseDate(final String date) throws TechnicalException {
		try {
			return SIMPLE_DATE_FORMAT.parse(date);
		} catch (final ParseException e) {
			e.printStackTrace();
			throw new TechnicalException("Erreur de parsing date", e);
		}
	}

	/**
	 * A completer
	 *
	 * @return
	 */
	public static String createUniqueId() {
		return UUID.randomUUID().toString();
	}

	/**
	 * A completer
	 *
	 * @param pIndividualContact
	 * @return
	 */
	public static String messageToSend(final IndividualContactVO pIndividualContact) {

		final String message = "{\"id_notification \" : \"" + pIndividualContact.getId() + "\",\"sender \":\""
				+ pIndividualContact.getFirstName()
				+ "\", \"receiver\" : \"\",\"object\" : \"\",\"content\" : \"\",\"message_type\" : \" Notification \",\"file_path_local\" : \"\",\"file_path_hdfs\" : \"/individual-contact/"
				+ pIndividualContact.getFirstName()
				+ ".json\",\"social_network_type\" : \"\",\"number_of_lines\" : \"\",\"owner\" : \"\",\"arrival_hour_gateway\" : \"\",\"start_processing_hour\" : \"\",\"end_processing_hour\" : \"\"}";

		return message;
	}
}
