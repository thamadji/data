package com.bnp.dao;

import java.util.List;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;

/**
 * IIndividualContactDao - Interface Individual Contact.
 *
 * @version 1.0
 * @see
 * @author
 * @copyright (C) 2016
 * @date 11/01/2016
 * @notes
 */
public interface IIndividualContactDao {

	/**
	 * addIndividualContact - ajoute un contact individuel dans ML
	 *
	 * @return vIndividualContact
	 * @param vIndividualContact
	 *            : contact individuel à ajouter
	 * @throws TechnicalException
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */
	public String addIndividualContact(IndividualContactVO vIndividualContact) throws TechnicalException;

	/**
	 * getIndividualContactList - recherche la liste des contacts individuels
	 * dans ML
	 *
	 * @return vIndividualContact : contact individuel
	 * @param
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */
	public List<IndividualContactVO> getIndividualContactList();

	/**
	 * getIndividualContactById -
	 * 
	 * @return vIndividualContact : contact individuel
	 * @param id
	 *            contact individuel
	 * @throws TechnicalException
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */
	public IndividualContactVO getIndividualContactById(String id) throws TechnicalException;

}
