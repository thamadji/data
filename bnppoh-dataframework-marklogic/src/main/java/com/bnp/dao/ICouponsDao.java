package com.bnp.dao;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.CouponVO;

/**
 * 
 * @author bdansoko
 *
 */
public interface ICouponsDao {

	/**
	 * 
	 * @param pCouponVO
	 * @return
	 * @throws TechnicalException
	 */
	public String addCoupon(CouponVO pCouponVO) throws TechnicalException;
	
	/**
	 * 
	 * @param pCouponID
	 * @return
	 */
	public CouponVO getCoupon(String pCouponID) throws TechnicalException;
	
}
