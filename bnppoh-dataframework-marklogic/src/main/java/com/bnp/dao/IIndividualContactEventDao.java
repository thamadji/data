package com.bnp.dao;

import com.bnp.exceptions.TechnicalException;

/**
 * 
 * @author thamadji
 *
 */
public interface IIndividualContactEventDao {
	public void sendIndividualContactInPointOfSales(String pIndividualContactId, String pPointOfSalesId, String pDeviceId) throws TechnicalException;
}
