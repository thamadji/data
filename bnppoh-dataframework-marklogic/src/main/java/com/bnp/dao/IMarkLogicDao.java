package com.bnp.dao;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.MarkLogicVO;

/**
 * A completer
 * @author jnairain
 *
 */
public interface IMarkLogicDao {

	/**
	 * A completer
	 * @param pMarkLogicVO
	 * @param pCollection
	 * @return
	 */
	public String write (MarkLogicVO pMarkLogicVO, String pCollection) throws TechnicalException;
	
	/**
	 * A completer
	 * @param pMarkLogicVOArray
	 * @param pCollection
	 */
	public void writeSet (MarkLogicVO[] pMarkLogicVOArray, String pCollection);
	
	/**
	 * A completer
	 * @param pDocumentId
	 * @param pCollection
	 * @return
	 */
	public MarkLogicVO getDocumentByUri(String pDocumentId, String pCollection) throws TechnicalException;
}
