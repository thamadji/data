package com.bnp.dao;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.DeviceVO;
/**
 * 
 * @author thamadji
 *
 */
public interface IDeviceDao {
	public String addDevice(DeviceVO pDevice) throws TechnicalException;
}
