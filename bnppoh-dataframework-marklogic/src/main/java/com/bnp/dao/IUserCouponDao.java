package com.bnp.dao;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.UserCouponVO;

/**
 * 
 * @author bdansoko
 *
 */
public interface IUserCouponDao {

	/**
	 * 
	 * @param pCouponId
	 * @param pIndividualContactId
	 * @return
	 * @throws TechnicalException
	 */
	public String addUserCoupon(UserCouponVO pUserCouponVO) throws TechnicalException;
	
	/**
	 * 
	 * @param pUserCouponID
	 * @return
	 * @throws TechnicalException
	 */
	public UserCouponVO getUserCoupon (String pUserCouponID) throws TechnicalException;
		
}
