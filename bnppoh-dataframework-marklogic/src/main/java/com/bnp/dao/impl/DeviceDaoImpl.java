package com.bnp.dao.impl;

import com.bnp.dao.IDeviceDao;
import com.bnp.dao.IMarkLogicDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.DeviceVO;
import com.bnp.tools.Constantes;

public class DeviceDaoImpl implements IDeviceDao {

	public String addDevice(DeviceVO pDevice) throws TechnicalException {
		IMarkLogicDao vMarkLogicDao = new MarkLogicDaoImpl();
		pDevice.setDocumentURI(Constantes.DEVICE_URI_PREFIX + pDevice.getDeviceId());
		
		return vMarkLogicDao.write(pDevice, "device");
	}
	
}
