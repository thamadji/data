package com.bnp.dao.impl;

import com.bnp.dao.IMarkLogicDao;
import com.bnp.dao.IPointOfSalesDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.PointOfSalesVO;
import com.bnp.tools.Constantes;

/**
 * A completer
 *
 * @author jnairain
 *
 */
public class PointOfSalesDaoImpl implements IPointOfSalesDao {

	// private static final Logger LOGGER =
	// Logger.getLogger(PointOfSalesDaoImpl.class);

	private final IMarkLogicDao mMarkLogicDao = new MarkLogicDaoImpl();

	/**
	 * A completer
	 *
	 * @throws TechnicalException
	 */
	@Override
	public String addPointOfSales(final PointOfSalesVO pPointOfSalesVO) throws TechnicalException {
		pPointOfSalesVO.setDocumentURI(Constantes.POINT_OF_SALES_URI_PREFIX + pPointOfSalesVO.getPointOfSalesID());
		return mMarkLogicDao.write(pPointOfSalesVO, Constantes.POINT_OF_SALES_COLLECTION);
	}

	/**
	 * A completer
	 *
	 * @throws TechnicalException
	 */

	@Override
	public PointOfSalesVO getPointOfSales(final String pPointOfSalesID) throws TechnicalException {
		final String vDocumentURI = Constantes.POINT_OF_SALES_URI_PREFIX + pPointOfSalesID;

		final PointOfSalesVO vPointOfSalesVO = (PointOfSalesVO) mMarkLogicDao.getDocumentByUri(vDocumentURI,
				Constantes.POINT_OF_SALES_COLLECTION);
		vPointOfSalesVO.setDocumentURI(vDocumentURI);

		return vPointOfSalesVO;

	}

}
