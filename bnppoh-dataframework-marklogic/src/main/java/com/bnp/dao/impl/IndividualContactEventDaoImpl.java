package com.bnp.dao.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import com.bnp.dao.IIndividualContactEventDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.WebServiceEntity;
import com.bnp.tools.Constantes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author thamadji
 *
 *
 *
 **/
public class IndividualContactEventDaoImpl implements IIndividualContactEventDao {
	Logger LOGGER = Logger.getLogger(IndividualContactDaoImpl.class);
	@Override
	public void sendIndividualContactInPointOfSales(final String pIndividualContactId, final String pPointOfSalesId,
			final String pDeviceId) throws TechnicalException {

		final WebServiceEntity entity = new WebServiceEntity(pIndividualContactId, pPointOfSalesId, pDeviceId);
	
		final HttpClient httpClient = HttpClients.createDefault();
		HttpResponse response = null;

		LOGGER.debug("Envoi de la requete sur l'url : " + Constantes.WS_EVENT_URI + Constantes.WS_EVENT_PATH);
		final HttpPost postRequest = new HttpPost(Constantes.WS_EVENT_URI + Constantes.WS_EVENT_PATH);

		final ObjectMapper objectMapper = new ObjectMapper();
		String entityJson = "";
		StringEntity input = null;
		try {
			entityJson = objectMapper.writeValueAsString(entity);
			entityJson = entityJson.replace("individualContactId", "IDUtilisateur");
			entityJson = entityJson.replace("pointOfSalesId", "IDCommerce");
			entityJson = entityJson.replace("deviceId", "IDDevice");
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		LOGGER.debug("Contenu du parsing json avant envoi = \n" + entityJson);

		try {
			input = new StringEntity(entityJson);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e);	
		}
		input.setContentType("application/json");
		postRequest.setEntity(input);
		try {
			System.out.println("POST sur le point d'etre execute");
			response = httpClient.execute(postRequest);
		} catch (final ClientProtocolException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e);	

		} catch (final IOException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e);	
		}

		if (response.getStatusLine().getStatusCode() != 204) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		} else {
			LOGGER.debug("POST effectué avec succès");
		}
		httpClient.getConnectionManager().shutdown();

	}

}
