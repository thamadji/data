package com.bnp.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.bnp.dao.IIndividualContactDao;
import com.bnp.dao.IMarkLogicDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.IndividualContactVO;
import com.bnp.tools.Constantes;

/**
 * IndividualContactDaoImpl - Implementation de la classe IIndividualContactDao.
 *
 * @version 1.0
 * @see
 * @author
 * @copyright (C) 2016
 * @date 11/01/2016
 * @notes
 */
public class IndividualContactDaoImpl implements IIndividualContactDao {
	private static final Logger LOGGER = Logger.getLogger(IndividualContactDaoImpl.class);
	/**
	 * addIndividualContact - ajoute un contact individuel dans ML
	 *
	 * @return
	 * @param vIndividualContact
	 *            : contact individuel à ajouter
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */

	/**
	 * @throws TechnicalException
	 *
	 */
	@Override
	public String addIndividualContact(final IndividualContactVO pIndividualContact) throws TechnicalException {

		final IMarkLogicDao vMarkLogicDao = new MarkLogicDaoImpl();
		pIndividualContact.setDocumentURI(Constantes.INDIVIDUAL_CONTACT_URI_PREFIX + pIndividualContact.getId());
		// Resolution conflit
		return vMarkLogicDao.write(pIndividualContact, Constantes.INDIVIDUAL_CONTACT_COLLECTION);

	}

	/**
	 * getIndividualContactById -
	 *
	 * @return vIndividualContact : contact individuel
	 * @param id
	 *            contact individuel
	 * @throws TechnicalException
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */
	@Override
	public IndividualContactVO getIndividualContactById(final String vId) throws TechnicalException {
		LOGGER.debug("Parametre dans le dao get by id :"+vId );
		final IMarkLogicDao pMarkLogicDao = new MarkLogicDaoImpl();
		IndividualContactVO pIndividualContact = new IndividualContactVO();
		// String pDocumentContent = "";
		pIndividualContact = (IndividualContactVO) pMarkLogicDao.getDocumentByUri(vId, "individualContact");
		LOGGER.debug("Contenu du POJO : \n"+pIndividualContact.toString());

		return pIndividualContact;

	}

	/**
	 * getIndividualContactList - recherche la liste des contacts individuels
	 * dans ML
	 *
	 * @return vIndividualContact : contact individuel
	 * @param
	 * @exception @see
	 * @date 11/01/2016
	 * @note
	 */
	@Override
	public List<IndividualContactVO> getIndividualContactList() {

		return null;
	}

}
