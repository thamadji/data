package com.bnp.dao.impl;

import org.apache.log4j.Logger;

import com.bnp.dao.IMarkLogicDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.AddressVO;
import com.bnp.model.BiTemporalVO;
import com.bnp.model.DeviceVO;
import com.bnp.model.GeoPoint2DVO;
import com.bnp.model.IndividualContactVO;
import com.bnp.model.LocalizationVO;
import com.bnp.model.MarkLogicVO;
import com.bnp.model.PointOfSalesVO;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.model.coupon.CouponVO;
import com.bnp.model.coupon.UserCouponVO;
import com.bnp.tools.Configuration;
import com.bnp.tools.Constantes;
import com.bnp.tools.DateConverter;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.Transaction;
import com.marklogic.client.document.DocumentManager.Metadata;
import com.marklogic.client.document.ServerTransform;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.DocumentMetadataHandle.DocumentCollections;
import com.marklogic.client.io.Format;
import com.marklogic.client.io.StringHandle;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * A completer
 *
 * @author jnairain
 *
 */
public class MarkLogicDaoImpl implements IMarkLogicDao {

	private static final Logger LOGGER = Logger.getLogger(MarkLogicDaoImpl.class);
	static XStream xstream = new XStream(new StaxDriver());

	static {
		xstream.processAnnotations(MarkLogicVO.class);
		xstream.processAnnotations(TriplesVO.class);
		xstream.processAnnotations(TripleVO.class);
		xstream.processAnnotations(PointOfSalesVO.class);
		xstream.processAnnotations(GeoPoint2DVO.class);
		xstream.processAnnotations(IndividualContactVO.class);
		xstream.processAnnotations(AddressVO.class);
		xstream.processAnnotations(LocalizationVO.class);
		xstream.processAnnotations(DeviceVO.class);
		xstream.processAnnotations(BiTemporalVO.class);
		xstream.processAnnotations(CouponVO.class);
		xstream.processAnnotations(UserCouponVO.class);

		final DateConverter vDateConverter = new DateConverter();
		xstream.registerConverter(vDateConverter);
		// xstream.ignoreUnknownElements();
	}

	/**
	 * A completer
	 */
	@Override
	public String write(final MarkLogicVO pMarkLogicVO, final String pCollection) throws TechnicalException {

		final DatabaseClient client = DatabaseClientFactory.newClient(Configuration.DB_HOST, Configuration.DB_PORT,
				Configuration.DB_USER_NAME, Configuration.DB_PASSWORD, Authentication.DIGEST);

		XMLDocumentManager docMgr = null;
		if (client != null) {
			docMgr = client.newXMLDocumentManager();
		}
		final DocumentMetadataHandle vMetadataHandle = new DocumentMetadataHandle();
		DocumentCollections vCollections = null;

		if (docMgr != null) {
			docMgr.setMetadataCategories(Metadata.COLLECTIONS);
		}

		String vXML = xstream.toXML(pMarkLogicVO);

		vXML = vXML.replaceAll("<triple>", "<sem:triple>");
		vXML = vXML.replaceAll("</triple>", "</sem:triple>");

		vXML = vXML.replaceAll("<sem:triples>", "<sem:triples xmlns:sem=\"http://marklogic.com/semantics\">");

		// If object has already an URI
		if (null != pMarkLogicVO.getDocumentURI()) {
			LOGGER.debug(pMarkLogicVO.getDocumentURI());
			Transaction tx = null;
			try {
				tx = client.openTransaction();
				LOGGER.debug("Inserting document : " + vXML);

				final StringHandle handle = new StringHandle(vXML).withFormat(Format.XML);
				vCollections = vMetadataHandle.getCollections();
				vCollections.addAll(pCollection);
				// docMgr.write(pMarkLogicVO.getDocumentURI(), vMetadataHandle,
				// handle); //, new
				// ServerTransform("transform_to_semantic_v1"));
				docMgr.write(pMarkLogicVO.getDocumentURI(), vMetadataHandle, handle,
						new ServerTransform("transform_to_semantic_v1"), null, Constantes.BITEMPORAL_COLLECTION); // ,
																													// new
																													// ServerTransform("transform_to_semantic_v1"));

				// batch.add(pMarkLogicVO.getDocumentURI(), handle);
				// docMgr.write(batch, new
				// ServerTransform("transform_to_semantic_v1"));

				tx.commit();
			} catch (final Exception ex) {
				if (null != tx)
					tx.rollback();
				LOGGER.debug("Error inserting document : " + vXML);
				ex.printStackTrace();
				throw new TechnicalException(ex);
			} finally {
				if (null != client)
					client.release();
			}
			LOGGER.debug("Successfully inserted document : " + pMarkLogicVO.getDocumentURI());
			return pMarkLogicVO.getDocumentURI();
		} else {
			// Object has no URI yet
			LOGGER.error("Document has no URI : " + vXML);
			throw new TechnicalException("Document has no URI");
		}
	}

	/**
	 * A completer
	 */
	@Override
	public void writeSet(final MarkLogicVO[] pMarkLogicVOArray, final String pCollection) {
		// TODO Auto-generated method stub
	}

	@Override
	public MarkLogicVO getDocumentByUri(final String pDocumentId, final String pCollection) throws TechnicalException {
		final DatabaseClient client = DatabaseClientFactory.newClient(Configuration.DB_HOST, Configuration.DB_PORT,
				Configuration.DB_USER_NAME, Configuration.DB_PASSWORD, Authentication.DIGEST);

		final DocumentMetadataHandle vMetadataHandle = new DocumentMetadataHandle();
		DocumentCollections vCollections = null;

		MarkLogicVO vMarkLogic = null;
		String vDocumentXML = "";
		final XMLDocumentManager docMgr = client.newXMLDocumentManager();
		final StringHandle stringHandle = new StringHandle();
		LOGGER.debug("Recuperation du document à partir de l'uri: " + pDocumentId + " ...");
		try {
			vCollections = vMetadataHandle.getCollections();
			vCollections.addAll(pCollection);
			vCollections.addAll("latest");
			vMetadataHandle.setCollections(vCollections);

			docMgr.read(pDocumentId, vMetadataHandle, stringHandle);
			vCollections = vMetadataHandle.getCollections();
			LOGGER.debug("Contenu du handler = " + stringHandle.get());
			vDocumentXML = stringHandle.get();
			LOGGER.debug("Collections du document = " + vCollections.toString());

			vDocumentXML = vDocumentXML.replaceAll("<sem:triple>", "<triple>");
			vDocumentXML = vDocumentXML.replaceAll("</sem:triple>", "</triple>");

			LOGGER.debug("Transformation du xml en pojo");
			vMarkLogic = (MarkLogicVO) xstream.fromXML(vDocumentXML);

			if (null == vMarkLogic.getTriples() || null == vMarkLogic.getTriples().getTripleList()) {
				vMarkLogic.setTriples(new TriplesVO());
			}
		} catch (final Exception e) {
			throw new TechnicalException(e);
		}

		return vMarkLogic;
	}

}
