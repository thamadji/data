package com.bnp.dao.impl;

import com.bnp.dao.IMarkLogicDao;
import com.bnp.dao.IUserCouponDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.UserCouponVO;
import com.bnp.tools.Constantes;

public class UserCouponDaoImpl implements IUserCouponDao {

	private IMarkLogicDao mMarkLogicDao = new MarkLogicDaoImpl();
	
	/**
	 * A completer
	 * @param pUserCouponVO
	 * @return
	 * @throws TechnicalException
	 */
	public String addUserCoupon(UserCouponVO pUserCouponVO) throws TechnicalException {
		pUserCouponVO.setDocumentURI(Constantes.USER_COUPON_URI_PREFIX + pUserCouponVO.getUserCouponId());
		return mMarkLogicDao.write(pUserCouponVO, Constantes.USER_COUPON_COLLECTION);
	}

	/**
	 * A completer
	 * @param pCouponsVOID
	 * @return
	 * @throws TechnicalException
	 */
	public UserCouponVO getUserCoupon(String pUserCouponID) throws TechnicalException{
		String vDocumentURI = Constantes.USER_COUPON_URI_PREFIX + pUserCouponID;

		UserCouponVO vUserCouponVO = (UserCouponVO) mMarkLogicDao.getDocumentByUri(vDocumentURI, Constantes.USER_COUPON_COLLECTION);
		vUserCouponVO.setDocumentURI(vDocumentURI);
		
		return vUserCouponVO;
	}
}
