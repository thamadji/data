package com.bnp.dao.impl;

import com.bnp.dao.ICouponsDao;
import com.bnp.dao.IMarkLogicDao;
import com.bnp.exceptions.TechnicalException;
import com.bnp.model.coupon.CouponVO;
import com.bnp.tools.Constantes;

public class CouponsDaoImpl implements ICouponsDao {

	private IMarkLogicDao mMarkLogicDao = new MarkLogicDaoImpl();
	
	/**
	 * @param pCouponVO
	 */

	public String addCoupon(CouponVO pCouponVO) throws TechnicalException {
		pCouponVO.setDocumentURI(Constantes.COUPON_URI_PREFIX + pCouponVO.getCouponId());
		return mMarkLogicDao.write(pCouponVO, Constantes.COUPON_COLLECTION);
	}

	/**
	 * @param  pCouponID
	 */

	public CouponVO getCoupon(String pCouponID) throws TechnicalException{
		String vDocumentURI = Constantes.COUPON_URI_PREFIX + pCouponID;
		
		CouponVO vCouponVO = (CouponVO) mMarkLogicDao.getDocumentByUri(vDocumentURI, Constantes.COUPON_COLLECTION);
		vCouponVO.setDocumentURI(vDocumentURI);
		
		return vCouponVO;
	}

}
