package com.bnp.exceptions;

/**
 * A completer
 * @author jnairain
 *
 */
public class FunctionalException extends Exception {
	/**
	 * A completer
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A completer
	 * @param message
	 */
	public FunctionalException(String message) {
		super(message);
	}
	
	/**
	 * A completer
	 * @param e
	 */
	public FunctionalException(Exception e) {
		super(e);
	}
}
