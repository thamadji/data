package com.bnp.exceptions;

/**
 * A completer
 *
 * @author jnairain
 *
 */
public class TechnicalException extends Exception {

	/**
	 * A completer
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A completer
	 *
	 * @param message
	 */
	public TechnicalException(final String message) {
		super(message);
	}

	/**
	 * A completer
	 *
	 * @param e
	 */
	public TechnicalException(final Exception e) {
		super(e);
	}

	/**
	 *
	 * @param msg
	 * @param e
	 */
	public TechnicalException(final String msg, final Exception e) {
		super(msg, e);
	}
}
