package com.bnp.exceptions;

public class IndividualContactException extends Exception {

	public final static String NOT_FOUND = "Contact individuel non existant en base";
	public final static String ALREADY_EXIST = "Contact individuel déjà existant en base";
	public final static String UNKNOWN = "Erreur non identifée";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IndividualContactException(String message) {
		super(message);
	}
	public IndividualContactException(Exception e) {
		super(e);
	}

}
