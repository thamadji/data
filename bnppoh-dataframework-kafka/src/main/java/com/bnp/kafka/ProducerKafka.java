package com.bnp.kafka;

import java.util.*;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;


public class ProducerKafka {

	public static void main(String[] args) {
	   String topic = args[0]; 
	   String message = args[1]; 
	   
	  // System.out.println(topic);
	  // System.out.println(message);
	   
	  // long events = Long.parseLong(args[0]);
       Random rnd = new Random();

       Properties props = new Properties();
       props.put("metadata.broker.list", "frparvm-mn.corp.capgemini.com:6667");
       props.put("serializer.class", "kafka.serializer.StringEncoder");
       props.put("partitioner.class", "com.bnp.kafka.SimplePartitioner");
       props.put("request.required.acks", "1");
       props.put("group.id", "group_01");

       ProducerConfig config = new ProducerConfig(props);

       Producer<String, String> producer = new Producer<String, String>(config);

       //for (long nEvents = 0; nEvents < events; nEvents++) { 
       //   long runtime = new Date().getTime();  
              String ip = "192.168.2." + rnd.nextInt(255); 
       //String temps = runtime + ",www.example.com," + ip; 
              String msg = message;
              
              KeyedMessage<String, String> data = new KeyedMessage<String, String>(topic, ip, msg);
              producer.send(data);
       //}
       producer.close();
    }
}