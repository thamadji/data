package com.bnp.kafka;

import java.util.*;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;


public class NotificationKafkaML {

	
	public NotificationKafkaML(){}
	public  void addNotificationKafkaML(String topic,String message){
	 	   
		    Random rnd = new Random();

		    Properties props = new Properties();
		    props.put("metadata.broker.list", "frparvm-mn.corp.capgemini.com:6667");
		    props.put("serializer.class", "kafka.serializer.StringEncoder");
		    props.put("partitioner.class", "com.bnp.kafka.SimplePartitioner");
		    props.put("request.required.acks", "1");
		    props.put("group.id", "group_01");

		    ProducerConfig config = new ProducerConfig(props);

		    Producer<String, String> producer = new Producer<String, String>(config);
		    		
		           String ip = "192.168.2." + rnd.nextInt(255); 
		 	       String msg = message;    
		           KeyedMessage<String, String> data = new KeyedMessage<String, String>(topic, ip, msg);
		           producer.send(data);

		    producer.close();

	}
}
