package com.bnp.kafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

public class ConsumerGroupExample {
	private static final Logger LOGGER = Logger.getLogger(ConsumerGroupExample.class);

	private final ConsumerConnector consumer;
	private final String topic = "test_topic";
	private ExecutorService executor;

	public ConsumerGroupExample() {
		consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig());

	}

	public void shutdown() {
		if (consumer != null)
			consumer.shutdown();
		if (executor != null)
			executor.shutdown();
		try {
			if (executor != null && !executor.awaitTermination(5000, TimeUnit.MILLISECONDS)) {
				LOGGER.debug("Timed out waiting for consumer threads to shut down, exiting uncleanly");
			}
		} catch (final InterruptedException e) {
			LOGGER.error("Interrupted during shutdown, exiting uncleanly", e);
		}
	}

	public void run(final int aNumThreads) {

		final Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(topic, Integer.valueOf(aNumThreads));
		final Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
		final List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);

		// now launch all the threads
		//
		executor = Executors.newFixedThreadPool(aNumThreads);

		// now create an object to consume the messages
		//
		int threadNumber = 0;
		for (final KafkaStream<byte[], byte[]> stream : streams) {
			executor.submit(new ConsumerTest(stream, threadNumber));
			threadNumber++;
		}
	}

	private static ConsumerConfig createConsumerConfig() {
		final Properties props = new Properties();
		props.put("zookeeper.connect",
				"frparvm-mn.corp.capgemini.com:2181,frparvm-dn-01.corp.capgemini.com:2181,frparvm-dn-02.corp.capgemini.com:2181");
		props.put("group.id", "group_A1");
		props.put("zookeeper.session.timeout.ms", "400");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");

		return new ConsumerConfig(props);
	}

	public static void main(final String[] args) {
		/*
		 * String zooKeeper = args[0]; String groupId = args[1]; String topic =
		 * args[2]; int threads = Integer.parseInt(args[3]);
		 */

		final ConsumerGroupExample example = new ConsumerGroupExample();
		example.run(1);

		try {
			Thread.sleep(10000);
		} catch (final InterruptedException ie) {

		}
		example.shutdown();
	}
}