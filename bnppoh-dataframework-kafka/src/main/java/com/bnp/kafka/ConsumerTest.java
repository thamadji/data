package com.bnp.kafka;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;

public class ConsumerTest implements Runnable {

	private final KafkaStream<byte[], byte[]> m_stream;
	private final int m_threadNumber;

	public ConsumerTest(final KafkaStream<byte[], byte[]> a_stream, final int a_threadNumber) {
		m_threadNumber = a_threadNumber;
		m_stream = a_stream;
	}

	@Override
	public void run() {
		final ConsumerIterator<byte[], byte[]> it = m_stream.iterator();
		while (it.hasNext())

			System.out.println("Thread " + m_threadNumber + ": " + new String(it.next().message()));

		System.out.println("Shutting down Thread: " + m_threadNumber);
	}
}