package com.bnp.kafka;

import kafka.producer.Partitioner;
import kafka.utils.VerifiableProperties;

public class SimplePartitioner implements Partitioner {
	public SimplePartitioner(final VerifiableProperties props) {

	}

	@Override
	public int partition(final Object key, final int aNumPartitions) {
		int partition = 0;
		final String stringKey = (String) key;
		final int offset = stringKey.lastIndexOf('.');
		if (offset > 0) {
			partition = Integer.parseInt(stringKey.substring(offset + 1)) % aNumPartitions;
		}
		return partition;
	}

}