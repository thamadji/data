package com.bnp.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.model.coupon.CouponPointOfsalesVO;
import com.bnp.model.coupon.CouponPurchaseCategoriesVO;
import com.bnp.model.coupon.CouponVO;
import com.bnp.model.coupon.CouponValueVO;
import com.bnp.services.CouponsServiceImpl;
import com.bnp.services.ICouponsService;
import com.bnp.tools.Constantes;

public class BatchCoupon {

	private static final Logger LOGGER = Logger.getLogger(BatchCoupon.class);

	/**
	 * A completer
	 *
	 * @param args
	 */
	public static void main(final String[] args) {

		final String vInputFile = "D:\\coupons.csv";
		File vFile;

		BufferedReader br = null;
		String vLine;

		try {
			vFile = new File(vInputFile);
			br = new BufferedReader(new FileReader(vInputFile));
			// Skip header
			vLine = br.readLine();
			// Read lines
			vLine = br.readLine();
			while (vLine != null) {
				System.out.println(vLine);
				final String[] vArrayOfFields = vLine.split(";");
				final CouponVO vCouponsVO = new CouponVO(vArrayOfFields[0],

						vArrayOfFields[1],

						vArrayOfFields[2],

						new CouponValueVO(vArrayOfFields[3]),

						new CouponPurchaseCategoriesVO(Arrays.asList(vArrayOfFields[4].split(","))),

						new CouponPointOfsalesVO(Arrays.asList(vArrayOfFields[5].split(","))),

						new TriplesVO(Arrays
								.asList(new TripleVO[] { new TripleVO(Constantes.COUPON_URI_PREFIX + vArrayOfFields[0],
										Constantes.PREDICAT_IS_FROM_FILE,
										Constantes.COUPON_FILE_URI_PREFIX + vFile.getName()) })));

				System.out.println(vCouponsVO);

				final ICouponsService vCouponsService = new CouponsServiceImpl();

				final String pURI = vCouponsService.addCoupon(vCouponsVO);
				LOGGER.debug("Inserted coupon " + pURI);
				System.out.println("Inserted coupon " + pURI);
				vLine = br.readLine();
			}

			// final ICouponsService vCouponsService = new CouponsServiceImpl();
			// CouponVO vCoupon = vCouponsService.getCoupon("1");

			//
		} catch (final FileNotFoundException fne) {
			LOGGER.error("File not found : " + vInputFile, fne);
			System.exit(1);
		} catch (final IOException ioe) {
			LOGGER.error("Error reading file : " + vInputFile, ioe);
			System.exit(1);
		} catch (final TechnicalException tex) {
			LOGGER.error("Error reading file : " + vInputFile, tex);
			System.exit(1);
		}
	}
}
