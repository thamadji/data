package com.bnp.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.bnp.exceptions.TechnicalException;
import com.bnp.model.LocalizationVO;
import com.bnp.model.OpenDaysVO;
import com.bnp.model.PointOfSalesVO;
import com.bnp.model.TripleVO;
import com.bnp.model.TriplesVO;
import com.bnp.services.IPointOfSalesService;
import com.bnp.services.PointOfSalesServiceImpl;
import com.bnp.tools.Constantes;

/**
 * A completer
 * 
 * @author jnairain
 *
 */
public class BatchPointOfSales {

	private static final Logger LOGGER = Logger.getLogger(BatchPointOfSales.class);

	/**
	 * A completer
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {

		final String vInputFile = "D:\\20160201_163000_pointOfSales.csv";
		File vFile;

		BufferedReader br = null;
		String vLine;

		try {
			vFile = new File(vInputFile);
			br = new BufferedReader(new FileReader(vInputFile));
			// Skip header
			vLine = br.readLine();

			// Service layer
			final IPointOfSalesService vPointOfSalesService = new PointOfSalesServiceImpl();

			// Read lines
			vLine = br.readLine();
			while (vLine != null) {
				System.out.println(vLine);
				final String[] vArrayOfFields = vLine.split(";");
				final String[] vArrayLocalisation = vArrayOfFields[6].split(",");
				final PointOfSalesVO vPointOfSalesVO = new PointOfSalesVO(vArrayOfFields[0],
						new LocalizationVO(vArrayLocalisation[0].trim(), vArrayLocalisation[1].trim()),
						vArrayOfFields[7], vArrayOfFields[1], Integer.parseInt(vArrayOfFields[2]),
						new OpenDaysVO(Arrays.asList(vArrayOfFields[5].split(","))),
						new TriplesVO(Arrays.asList(
								new TripleVO[] { new TripleVO(Constantes.POINT_OF_SALES_URI_PREFIX + vArrayOfFields[0],
										Constantes.PREDICAT_IS_FROM_FILE,
										Constantes.POINT_OF_SALES_FILE_URI_PREFIX + vFile.getName()) })));

				final String pURI = vPointOfSalesService.addPointOfSales(vPointOfSalesVO);
				LOGGER.debug("Inserted point of sales " + pURI);
				System.out.println("Inserted point of sales " + pURI);
				vLine = br.readLine();
			}

		} catch (final FileNotFoundException fne) {
			LOGGER.error("File not found : " + vInputFile, fne);
			System.exit(1);
		} catch (final IOException ioe) {
			LOGGER.error("Error reading file : " + vInputFile, ioe);
			System.exit(1);
		} catch (final TechnicalException tex) {
			LOGGER.error("Error reading file : " + vInputFile, tex);
			System.exit(1);
		}

	}

}
